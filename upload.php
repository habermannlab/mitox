<?php 
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}
if ($_GET['sessionid']) { 
        session_id($_GET['sessionid']);
}
session_start();
$id = session_id();

setcookie('mitox_session_id',$id,time() + (86400 * 7));

?>
<!DOCTYPE html>
<html lang="en">


<head>
    <title>mitoXplorer - Upload</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/main.css" rel="stylesheet" >

    <link rel="icon" type="image/png" href="img/logos/favicon.png">
</head>

<script>

function contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}
    
function validateForm(warningDivname) {
    
    //Javasciprt API for file size checking

    ///This part has to be modified!!!!
    //Duplicate name, file size, empty files etc
    var warningDiv = document.getElementById(warningDivname)
    
    warningDiv.style.display = "none";

    while (warningDiv.firstChild) {
        warningDiv.removeChild(warningDiv.firstChild);
    }
    
    var newdiv;
    
    if (warningDivname == "warning-div"){

        var organism = document.getElementById('organism').value;
        if (organism == ""){
            newdiv = document.createElement('div');
            newdiv.innerHTML = "<font color='red' size=4px>- Select organism</font>"
            warningDiv.appendChild(newdiv);
        }

        var exp = document.getElementById("expfile").value;
        if(exp == ""){
            newdiv = document.createElement('div');
            newdiv.innerHTML = "<font color='red' size=4px>- Expression file is missing</font>"
            warningDiv.appendChild(newdiv);
        }else{
            if (exp.substr(exp.lastIndexOf('.')+1) !== "csv" && exp.substr(exp.lastIndexOf('.')+1) !== "txt"){
                newdiv = document.createElement('div');
                newdiv.innerHTML = "<font color='red' size=4px>- The expression file is not a csv/txt file</font>"
                warningDiv.appendChild(newdiv);
            }
            
            var x = document.getElementsByClassName("must-fill-exp");
            for (i = 0; i < x.length; i++) {
                var name = x[i].getAttribute("displayName")
                if (x[i].value == "None" && x[i].parentElement.style.display != "none"){
                    newdiv = document.createElement('div');
                    newdiv.innerHTML = "<font color='red' size=4px>- Please enter the column number for "+name+" of the expression file</font>"
                    warningDiv.appendChild(newdiv);
                }
            }
                 
        }
        
        var mut = document.getElementById("mutfile").value;
        
        if(mut !== ""){
            if (mut.substr(mut.lastIndexOf('.')+1) !== "csv" && mut.substr(mut.lastIndexOf('.')+1) !== "txt"){
                newdiv = document.createElement('div');
                newdiv.innerHTML = "<font color='red' size=4px>- The mutation file is not a csv/txt file</font>"
                warningDiv.appendChild(newdiv);
            }
            
            var x = document.getElementsByClassName("must-fill-mut");
            for (i = 0; i < x.length; i++) {
                var name = x[i].getAttribute('displayName');
                if (x[i].value == "None" && x[i].parentElement.style.display != "none"){
                    newdiv = document.createElement('div');
                    newdiv.innerHTML = "<font color='red' size=4px>- Please enter the column number for "+name+" of the mutation file</font>"
                    warningDiv.appendChild(newdiv);
                }
            }

        }

    }else{


        var organism = document.getElementById("organismN").value;
        if (organism == ""){
            newdiv = document.createElement('div');
            newdiv.innerHTML = "<font color='red' size=4px>- Select organism</font>"
            warningDiv.appendChild(newdiv);
        }
        
        var repeat;
        x = document.getElementsByClassName("samplename");

        var samplename = []
        for (i = 0; i < x.length; i++) {
            if (contains(samplename,x[i].value)){
                repeat = i+1;
                newdiv = document.createElement('div');
                newdiv.innerHTML = "<font color='red' size=4px>- Please give all datasets a unique name</font>"
                warningDiv.appendChild(newdiv);
            }
            samplename.push(x[i].value);
        }

        var emptysample;
        x = document.getElementsByClassName("samplename");
        for (i = 0; i < x.length; i++) {
            if(x[i].value == ""){
                emptysample = i+1;
                newdiv = document.createElement('div');
                newdiv.innerHTML = "<font color='red' size=4px>- dataset name is missing (Sample "+(i+1)+")</font>"
                warningDiv.appendChild(newdiv);
            }
        }
        
        var exp;
        x = document.getElementsByClassName("expfile");
        for (i = 0; i < x.length; i++) {
            exp = x[i].value;
            
            if(exp == ""){
                newdiv = document.createElement('div');
                newdiv.innerHTML = "<font color='red' size=4px>- Expression file is missing (Sample "+(i+1)+")</font>"
                warningDiv.appendChild(newdiv);
            }else if(exp.substr(exp.lastIndexOf('.')+1) !== "csv" && exp.substr(exp.lastIndexOf('.')+1) !== "txt"){
                newdiv = document.createElement('div');
                newdiv.innerHTML = "<font color='red' size=4px>- The expression file is not a csv/txt file (Sample "+(i+1)+")</font>"
                warningDiv.appendChild(newdiv);
            }
        }

        var x = document.getElementsByClassName("must-fill-expN");
        for (i = 0; i < x.length; i++) {
            var name = x[i].name;
            if (x[i].value == "None" && x[i].parentElement.style.display != "none"){
                console.log(name);
                newdiv = document.createElement('div');
                newdiv.innerHTML = "<font color='red' size=4px>- Please enter the column number for "+name+"</font>"
                warningDiv.appendChild(newdiv);
            }
        }

        var mut;
        x = document.getElementsByClassName("mutfile");
        for (i = 0; i < x.length; i++) {
            mut = x[i].value;
            
            if (mut != "") {
            
                if(mut.substr(mut.lastIndexOf('.')+1) !== "csv" && mut.substr(mut.lastIndexOf('.')+1) !== "txt"){
                    newdiv = document.createElement('div');
                    newdiv.innerHTML = "<font color='red' size=4px>- The mutation file is not a csv/txt file (Sample "+(i+1)+")</font>"
                    warningDiv.appendChild(newdiv);
                    
                }

                var x = document.getElementsByClassName("must-fill-mutN");
                for (i = 0; i < x.length; i++) {
                    var name = x[i].name;
                    if (x[i].value == "None" && x[i].parentElement.style.display != "none"){
                        newdiv = document.createElement('div');
                        newdiv.innerHTML = "<font color='red' size=4px>- Please enter the column number for "+name+"</font>"
                        warningDiv.appendChild(newdiv);
                    }
                }
            }
        }

    }   
    
    if (warningDiv.firstChild) {
        warningDiv.style.display = "block";
        return false;
    }  
    
} 

var counter;
var limit = 10;

function addInput(divName){
    
    var inputdiv = document.getElementById(divName).childNodes;
    
    counter = inputdiv.length-1;

     if (counter == limit)  {
         var warningDiv = document.getElementById('warning-div')
         warningDiv.style.display = "block";
         newdiv = document.createElement('div');
         newdiv.innerHTML = "<font color='red'>You have reached the limit of adding 10 datasets</font>"
         warningDiv.appendChild(newdiv);
     }
     else {
          var newdiv = document.createElement('div');
         newdiv.className = "jumbotron";
         newdiv.style="border:solid #d3d1d1";
          newdiv.innerHTML = "<b>dataset name:</b> <input type='text' class='form-control samplename' name='samplename[]'><br> <b>Expression file:</b> <input name='expfile[]' type='file' class='expfile'><br> <b>Mutation file:</b> <input name='mutfile[]' type='file' class='mutfile'><br><button type='button' class='btn btn-default btn-xs' onclick='this.parentNode.parentNode.removeChild(this.parentNode)' style='float: right;'>Remove dataset</button>"
          document.getElementById(divName).appendChild(newdiv);
         document.getElementById("counter").value=counter;

     }
}

function radioClick(thisRadio){
    var target, targetvs;
    if (thisRadio.value == "yes") {
        target = "withSampleID"
        targetvs = "noSampleID"
        organismChange("organism")
    }
    else {
        target = "noSampleID";
        targetvs = "withSampleID"
        organismChange("organismN")
    }
    document.getElementById(target).style.display = "";
    document.getElementById(targetvs).style.display = "none";
}

function organismChange(thisDiv){
    var target, 
        targetvs = "org_col"
    var e = document.getElementById(thisDiv);
    var selectedOrganism = e.options[e.selectedIndex].value;
    if (selectedOrganism == "Fly") target = "fly_col";
    if (selectedOrganism == "Human") target = "human_col";
    if (selectedOrganism == "Mouse") target = "mouse_col";
    if (selectedOrganism == "BuddingYeast") target = "yeast_col";

    var elements = document.getElementsByClassName(targetvs);
    for (var i in elements) {
      if (elements.hasOwnProperty(i)) {
        elements[i].style = 'display:none';
      }
    }

    var elements = document.getElementsByClassName(target);
    for (var i in elements) {
      if (elements.hasOwnProperty(i)) {
        elements[i].style = 'display:';
      }
    }

}

function identifierChange(thisDiv,thisGeneIDs){
    

    var e = document.getElementById(thisDiv);
    var selectedID = e.options[e.selectedIndex].value;    

    var elements = document.getElementsByClassName(thisGeneIDs);
    for (var i in elements) {
      if (elements.hasOwnProperty(i)) {
        elements[i].style = 'display:none';
      }
    }

    document.getElementById(selectedID).style = 'display:'
    console.log(document.getElementById(selectedID))

}


</script>
    
<body>
  
<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">mitoXplorer 1.0</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="index.php"></a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="interactome.php">Interactome</a>
                    </li>
                    <li>
                        <a href="database.php">Database</a>
                    </li>
                    <li>
                        <a href="compare.php">Analysis</a>
                    </li>
                    <li>
                        <a href="#" style="background-color: #12e2c0;border-radius: 3px;color:white">Upload</a>
                    </li>
                    <li>
                    	<a href="funding.html">Funding</a>
                    </li>
                     <li>
                        <a href="feedback.html">Feedback</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
<div class="container">
   
    
    <!--<img class="img-responsive" src="mitomodel.png" width="297" align="left" background = "#EEEEEE">-->
    
    <div class="jumbotron">
        <br>
        <h2>DATA UPLOAD PORTAL</h2><br>
        <p>Expression and mutation data can be uploaded and visualised using our tool to guide your analysis.<br><br>
        Generate your data in the format described <u><a href="#Expression format">below</a></u>.<br>
        <!--Alternatively, use our recommended <u><b><a href="#">RNA-seq pipeline</a></b></u> for your data that produces both the expression and mutation data files.</p><br>-->

        <p>My data files...
        <div class="radio" style="font-size: 1.2em">
            <label><input type="radio" name="optradio" onclick="radioClick(this)" value="yes" checked>Include dataset ID</label>
        </div>
        <div class="radio" style="font-size: 1.2em">
            <label><input type="radio" name="optradio" onclick="radioClick(this)" value="no">Do not include dataset ID</label>
        </div>
        </p> 
    </div>

<div id="withSampleID">
    <form enctype="multipart/form-data" action="upload/uploader.php?sessionid=<?php echo $id; ?>" onsubmit="return validateForm('warning-div')" method="POST">
        <div class="jumbotron">
            <h3>SELECT ORGANISM</h3>
                <p>Either human, mouse, fly or budding yeast data are accepted</p>
                <select id="organism" name="organism" class="form-control" onchange="organismChange('organism')">
                        <option value="Human">Human</option>
                        <option value="Mouse">Mouse</option>
                        <option value="Fly">Fly</option>
                        <option value="BuddingYeast">BuddingYeast</option>
                </select>   
        </div>
        
        <div class="jumbotron">
            <h3>UPLOAD DATA</h3>
            <p><font size=4px>Submit EXPRESSION (<u><a href="#Expression format">example</a></u>) and MUTATION (optional - <u><a href="#Mutation format">example</a></u>) data in defined formats. Choose the column number of your file that corresponds to each column below. You could upload multiple files at the same time, or a single file that contains all the datasets. Fields marked with * have to be present in your file.</font></p>
            <input type="hidden" name="nfiles" value="n" id="nfiles">

            <b>Expression file:</b>
            <input name="expression[]" type="file" multiple="multiple" accept="text/csv, text/plain" id="expfile"><br>
            <!--<label style="padding-left: 15px; text-index:-15px"><input id="expHeader" type="checkbox" />With header</label>-->
            <div class="container">
                <div class="row">
                        <table class="table">
                        <thead>
                        <tr>
                            <th>Type of Identifier*</th>
                            <th>Column number of: </th>
                            <th>Dataset ID*</th>
                            <th>Gene Identifier*</th>
                            <th>Expression of Wild type/Control</th>
                            <th>Expression of Mutant/Tumor</th>
                            <th>Log2 Fold Change*</th>
                            <th>p-value</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td class="human_col org_col"><select class="must-fill-exp" id="identifier-exp-human" onchange="identifierChange('identifier-exp-human','geneIDs-exp')">
                            <option value="geneID-exp">HGNC Symbol</option>
                            <option value="geneID_a1-exp">HGNC ID</option>
                            <option value="geneID_a2-exp">ENSG ID</option>
                            <option value="geneID_a3-exp">NCBI ID</option>
                        </select></td>

                        <td class="mouse_col org_col" style="display:none"><select class="must-fill-exp" id="identifier-exp-mouse" onchange="identifierChange('identifier-exp-mouse','geneIDs-exp')">
                            <option value="geneID-exp">MGI Symbol</option>
                            <option value="geneID_a1-exp">ENSM ID</option>
                            <option value="geneID_a2-exp">NCBI ID</option>
                        </select></td>

                        <td class="fly_col org_col" style="display:none"><select class="must-fill-exp" id="identifier-exp-fly" onchange="identifierChange('identifier-exp-fly','geneIDs-exp')">
                            <option value="geneID-exp">Flybase GeneID</option>
                            <option value="geneID_a1-exp">Gene Symbol</option>
                        </select></td>

                        <td class="yeast_col org_col" style="display:none"><select class="must-fill-exp" id="identifier-exp-yeast" onchange="identifierChange('identifier-exp-yeast','geneIDs-exp')">
                            <option value="geneID-exp">Standard Name</option>
                            <option value="geneID_a1-exp">Systematic Name</option>
                            <option value="geneID_a2-exp">SGD ID</option>
                        </select></td>
                        
                            <td></td>

                            <td><select class="must-fill-exp" name="sampleID" displayName="Sample ID">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID-exp" class="geneIDs-exp"><select class="must-fill-exp" name="geneID" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a1-exp" class="geneIDs-exp" style="display:none"><select class="must-fill-exp" name="geneID_a1" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a2-exp" class="geneIDs-exp" style="display:none"><select class="must-fill-exp" name="geneID_a2">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a3-exp" class="geneIDs-exp" style="display:none"><select class="must-fill-exp" name="geneID_a3">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td><select name="normal">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="abnormal" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td> <select class="must-fill-exp" name="log2" displayName="Log 2 Fold Change value">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="pvalue">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                        </tr>  
                    </tbody>
                    </table>
                </div>
            </div>
            
            <b>Mutation file (optional):</b>
            <input name="mutation[]" type="file" multiple="multiple" accept="text/csv, text/plain" id="mutfile"><br>
            <div class="container">
                <div class="row">
                        <table class="table">
                        <thead>
                        <tr>
                            <th>Type of Identifier*</th>
                            <th>Column number of: </th>
                            <th>Dataset ID*</th>
                            <th>Gene Identifier*</th>
                            <th>Chromosome*</th>
                            <th>Position*</th>
                            <th>Reference Allele*</th>
                            <th>Alternative Allele*</th>
                            <th>Effect</th>
                            <th>Consequence</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td class="human_col org_col"><select class="must-fill-mut" id="identifier-mut-human" onchange="identifierChange('identifier-mut-human','geneIDs-mut')">
                            <option value="geneID-mut">HGNC Symbol</option>
                            <option value="geneID_a1-mut">HGNC ID</option>
                            <option value="geneID_a2-mut">ENSG ID</option>
                            <option value="geneID_a3-mut">NCBI ID</option>
                        </select></td>

                        <td class="mouse_col org_col" style="display:none"><select class="must-fill-mut" id="identifier-mut-mouse" onchange="identifierChange('identifier-mut-mouse','geneIDs-mut')">
                            <option value="geneID-mut">MGI Symbol</option>
                            <option value="geneID_a1-mut">ENSM ID</option>
                            <option value="geneID_a2-mut">NCBI ID</option>
                        </select></td>

                        <td class="fly_col org_col" style="display:none"><select class="must-fill-mut" id="identifier-mut-fly" onchange="identifierChange('identifier-mut-fly','geneIDs-mut')">
                            <option value="geneID-mut">Flybase GeneID</option>
                            <option value="geneID_a1-mut">Gene Symbol</option>
                        </select></td>

                        <td class="yeast_col org_col" style="display:none"><select class="must-fill-mut" id="identifier-mut-yeast" onchange="identifierChange('identifier-mut-yeast','geneIDs-mut')">
                            <option value="geneID-mut">Standard Name</option>
                            <option value="geneID_a1-mut">Systematic Name</option>
                            <option value="geneID_a2-mut">SGD ID</option>
                        </select></td>
                  
                            <td></td>
                            <td><select class="must-fill-mut" name="sampleIDMut" displayName="Sample ID">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td id="geneID-mut" class="geneIDs-mut"><select class="must-fill-mut" name="geneIDMut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a1-mut" class="geneIDs-mut" style="display:none"><select class="must-fill-mut" name="geneID_a1Mut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a2-mut" class="geneIDs-mut" style="display:none"><select class="must-fill-mut" name="geneID_a2Mut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a3-mut" class="geneIDs-mut" style="display:none"><select class="must-fill-mut" name="geneID_a3Mut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td> <select class="must-fill-mut" name="chrom" displayName="Chromosome">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select class="must-fill-mut" name="pos" displayName="Position">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            
                            <td><select class="must-fill-mut" name="ref" displayName="Reference Allele">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select class="must-fill-mut" name="alt" displayName="Alternative Allele">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="eff" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="consq" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        
            <div style = "display:none" id = "warning-div"></div><br>
            
            <button type="submit" class="btn btn-success" value="Upload File">Submit</button>
        </div>
    </form>
</div>

<div id="noSampleID" style="display:none">
    <form enctype="multipart/form-data" action="upload/uploader.php?sessionid=<?php echo $id; ?>" onsubmit="return validateForm('warning-div-noID')" method="POST">
        <div class="jumbotron">
            <h3>SELECT ORGANISM</h3>
            
                <p>Either human, mouse, fly or budding yeast data are accepted</p>
                <select id="organismN" name="organism" class="form-control" onchange="organismChange('organismN')">
                        <option value="Human">Human</option>
                        <option value="Mouse">Mouse</option>
                        <option value="Fly">Fly</option>
                        <option value="BuddingYeast">BuddingYeast</option>
                </select>   
        </div>
    

        <div class="jumbotron">
            <h3>UPLOAD DATA</h3>
            <p><font size=4px>Submit EXPRESSION (<u><a href="#Expression format">example</a></u>) and MUTATION (optional - <u><a href="#Mutation format">example</a></u>) data in defined formats. Choose the column number of your file that corresponds to each column below. Enter the dataset ID for the file and choose the corresponding expression (and mutation) file. Each file should contain only ONE dataset. Additional files could be added by clicking "Add more datasets". Fields marked with * has to be present in your file.</font></p>
            <input type="hidden" name="nfiles" value="y" id="nfiles">
            <input type="hidden" name="counter" value=1 id="counter">
            <input type="hidden" name="sampleID" value="None" id="sampleID">
            <input type="hidden" name="sampleIDMut" value="None" id="sampleIDMut">

            <b>Expression file:</b>
            <div class="container">
                <div class="row">
                        <table class="table">
                        <thead>
                        <tr>
                            <th>Type of Identifier*</th>
                            <th>Column number of: </th>
                            <th>Gene Identifier*</th>
                            <th>Expression of Wild type/Control</th>
                            <th>Expression of Mutant/Tumor</th>
                            <th>Log2 Fold Change*</th>
                            <th>p-value</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        <td class="human_col org_col"><select class="must-fill-expN" id="identifier-expN-human" onchange="identifierChange('identifier-expN-human','geneIDs-expN')">
                            <option value="geneID-expN">HGNC Symbol</option>
                            <option value="geneID_a1-expN">HGNC ID</option>
                            <option value="geneID_a2-expN">ENSG ID</option>
                            <option value="geneID_a3-expN">NCBI ID</option>
                        </select></td>

                        <td class="mouse_col org_col" style="display:none"><select class="must-fill-expN" id="identifier-expN-mouse" onchange="identifierChange('identifier-expN-mouse','geneIDs-expN')">
                            <option value="geneID-expN">MGI Symbol</option>
                            <option value="geneID_a1-expN">ENSM ID</option>
                            <option value="geneID_a2-expN">NCBI ID</option>
                        </select></td>

                        <td class="fly_col org_col" style="display:none"><select class="must-fill-expN" id="identifier-expN-fly" onchange="identifierChange('identifier-expN-fly','geneIDs-expN')">
                            <option value="geneID-expN">Flybase GeneID</option>
                            <option value="geneID_a1-expN">Gene Symbol</option>
                        </select></td>

                        <td class="yeast_col org_col" style="display:none"><select class="must-fill-expN" id="identifier-expN-yeast" onchange="identifierChange('identifier-expN-yeast','geneIDs-expN')">
                            <option value="geneID-expN">Standard Name</option>
                            <option value="geneID_a1-expN">Systematic Name</option>
                            <option value="geneID_a2-expN">SGD ID</option>
                        </select></td>

                            <td></td>

                            <td id="geneID-expN" class="geneIDs-expN"><select class="must-fill-expN" name="geneID" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a1-expN" class="geneIDs-expN" style="display:none"><select class="must-fill-expN" name="geneID_a1" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a2-expN" class="geneIDs-expN" style="display:none"><select class="must-fill-expN" name="geneID_a2" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a3-expN" class="geneIDs-expN" style="display:none"><select class="must-fill-expN" name="geneID_a3" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td><select name="normal" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="abnormal" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td> <select class="must-fill-expN" name="log2" displayName="Log 2 Fold Change value">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="pvalue">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                        </tr>  
                    </tbody>
                    </table>
                </div>
            </div>
            
            <b>Mutation file (optional):</b>
            <div class="container">
                <div class="row">
                        <table class="table">
                        <thead>
                        <tr>
                            <th>Type of Identifier*</th>
                            <th>Column number of: </th>
                            <th>Gene Identifier*</th>
                            <th>Chromosome*</th>
                            <th>Position*</th>
                            <th>Reference Allele*</th>
                            <th>Alternative Allele*</th>
                            <th>Effect</th>
                            <th>Consequence</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td class="human_col org_col"><select class="must-fill-mutN" id="identifier-mutN-human" onchange="identifierChange('identifier-mutN-human','geneIDs-mutN')">
                            <option value="geneID-mutN">HGNC Symbol</option>
                            <option value="geneID_a1-mutN">HGNC ID</option>
                            <option value="geneID_a2-mutN">ENSG ID</option>
                            <option value="geneID_a3-mutN">NCBI ID</option>
                        </select></td>

                        <td class="mouse_col org_col" style="display:none"><select class="must-fill-mutN" id="identifier-mutN-mouse" onchange="identifierChange('identifier-mutN-mouse','geneIDs-mutN')">
                            <option value="geneID-mutN">MGI Symbol</option>
                            <option value="geneID_a1-mutN">ENSM ID</option>
                            <option value="geneID_a2-mutN">NCBI ID</option>
                        </select></td>

                        <td class="fly_col org_col" style="display:none"><select class="must-fill-mutN" id="identifier-mutN-fly" onchange="identifierChange('identifier-mutN-fly','geneIDs-mutN')">
                            <option value="geneID-mutN">Flybase GeneID</option>
                            <option value="geneID_a1-mutN">Gene Symbol</option>
                        </select></td>

                        <td class="yeast_col org_col" style="display:none"><select class="must-fill-mutN" id="identifier-mutN-yeast" onchange="identifierChange('identifier-mutN-yeast','geneIDs-mutN')">
                            <option value="geneID-mutN">Standard Name</option>
                            <option value="geneID_a1-mutN">Systematic Name</option>
                            <option value="geneID_a2-mutN">SGD ID</option>
                        </select></td>

                            <td></td>

                            <td id="geneID-mutN" class="geneIDs-mutN"><select class="must-fill-mutN" name="geneIDMut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a1-mutN" class="geneIDs-mutN" style="display:none"><select class="must-fill-mutN" name="geneID_a1Mut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a2-mutN" class="geneIDs-mutN" style="display:none"><select class="must-fill-mutN" name="geneID_a2Mut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>

                            <td id="geneID_a3-mutN" class="geneIDs-mutN" style="display:none"><select class="must-fill-mutN" name="geneID_a3Mut" displayName="Gene Identifier">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td> <select class="must-fill-mutN" name="chrom" displayName="Chromosome">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select class="must-fill-mutN" name="pos" displayName="Position">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            
                            <td><select class="must-fill-mutN" name="ref" displayName="Reference Allele">
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select class="must-fill-mutN" name="alt" displayName="Alternative Allele" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="eff" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                            
                            <td><select name="consq" >
                            <option value="None">None</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option> 
                            <option value="20">20</option> 
                            </select></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>

            <div id="dynamicInput">
            
                <div class="jumbotron" style="border:solid #d3d1d1">
                    <b>Dataset name:</b>
                    <input type="text" class="form-control samplename" name="samplename[]"><br>
                    <b>Expression file:</b>
                        <input name="expfile[]" type="file" class="expfile"><br>
                    <b>Mutation file:</b>
                    <input name="mutfile[]" type="file" class="mutfile">
                </div> 

            </div>
            
            
            <div style = "display:none" id ="warning-div-noID"></div><br>
                
            <button type="submit" class="btn btn-success" value="Upload File">Submit</button>
            
            <button type="button" class="btn btn-info" style="float:right" onClick="addInput('dynamicInput');">Add more datasets</button>

        </div>
    
    </form> 

</div>




        
    <a name="Expression format"></a>
    <div class="jumbotron">
        <h3>Expression File Format</h3>
        <p><font size=4px>Only comma-separated-values (.csv) and tab-delimited (.txt) files are accepted. Below is an example for the expression file in table format.<br>
            Or download a dataset file <a href="./upload/test_exp/sample_exp.txt">here</a>.</font> <font size=-1em>(Tab-delimited file containing a single human dataset with HGNC symbol as Gene Identifier)</font></p>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Dataset ID+</th>
                    <th>Gene Identifier^</th>
                    <th>Control</th>
                    <th>Mutant</th>
                    <th>Log2Fold*</th>
                    <th>p-value</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>dataset1</td>
                    <td>MT-CO2</td>
                    <td>5.14185</td>
                    <td>0.525591</td>
                    <td>-3.29027</td>
                    <td>0.04165</td>
                </tr>
                <tr>
                    <td>dataset1</td>
                    <td>EPHB2</td>
                    <td>21.6398</td>
                    <td>77.9794</td>
                    <td>1.84941</td>
                    <td>0.03615</td>
                </tr>
                <tr>
                    <td>dataset1</td>
                    <td>CD52</td>
                    <td>113.979</td>
                    <td>24.01</td>
                    <td>-2.24706</td>
                    <td>0.14155</td>
                </tr>
                <tr>
                    <td>dataset1</td>
                    <td>MFSD2A</td>
                    <td>2.671</td>
                    <td>16.7198</td>
                    <td>2.64611</td>
                    <td>0.0318</td>
                </tr>
            </tbody>
        </table>
        <p><font size=4px>
        Note: <br>
        + Dataset IDs have to be present if you have chosen to include them at the upload portal above<br>
        ^ The type of gene identifier could be chosen at the upload portal above<br>
        * Fields marked with * have to be present in your file</font></p>
    </div>
    <a name="Mutation format"></a>
    <div class="jumbotron">
        <h3>Mutation File Format</h3>
        <p><font size=4px>Only comma-separated-values (.csv) and tab-delimited (.txt) files are accepted. Below is an example for the mutation file in table format.<br>
            Or download a dataset file <a href="./upload/test_mut/sample_mut.txt">here</a>.</font><font size=-1em> (Tab-delimited file containing a single human dataset with HGNC symbol as Gene Identifier)</font></p></p>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Dataset ID+</th>
                    <th>Gene Identifier^</th>
                    <th>Chromosome*</th>
                    <th>Position*</th>
                    <th>Reference Allele*</th>
                    <th>Alternative Allele*</th>
                    <th>Mutation Type (Effect)</th>
                    <th>Consequence</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>dataset1</td>
                    <td>MT-CO2</td>
                    <td>M</td>
                    <td>8159</td>
                    <td>T</td>
                    <td>C</td>
                    <td>SNP</td>
                    <td>Missense</td>
                </tr>
                <tr>
                    <td>dataset1</td>
                    <td>AGO1</td>
                    <td>1</td>
                    <td>36391661</td>
                    <td>TGAA</td>
                    <td>T</td>
                    <td>DEL</td>
                    <td>3'UTR</td>
                </tr>
                <tr>
                    <td>dataset1</td>
                    <td>ITIH1</td>
                    <td>3</td>
                    <td>52825585</td>
                    <td>T</td>
                    <td>C</td>
                    <td>SNP</td>
                    <td>Silent</td>
                </tr>
                <tr>
                    <td>dataset1</td>
                    <td>GPR35</td>
                    <td>2</td>
                    <td>241566012</td>
                    <td>G</td>
                    <td>A</td>
                    <td>SNP</td>
                    <td>Intron</td>
                </tr>
            </tbody>
        </table>
        <p><font size=4px>
        Note: <br>
        + Dataset IDs have to be present if you have chosen to include them at the upload portal above<br>
        ^ The type of gene identifier could be chosen at the upload portal above<br>
        * Fields marked with * have to be present in your file</font></p>
    </div>
        
    </div>     
</body>

</html>
