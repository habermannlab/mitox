<?php

    $to = "bianca.habermann@univ-amu.fr"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address

 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        header( "refresh:5;url=feedback.html" );
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['message'])) 
    {
        died('Empty message, cannot be sent!');       
    }
 
    $object = $_POST['object'];// required
    $institute = $_POST['institute'];// not required
    $text = $_POST['message'];// required
    $text = str_replace("\n.", "\n..", $text);
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(strlen($text) < 2) {
    $error_message .= 'The Comments you entered do not appear to be valid. Too short? <br />';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }

    $subject = "MitoXplorer form submission, topic: " . $object;
    $subject2 = "Copy of your Mitoxplorer form submission, topic: " . $object;

    if($institute){
    	$message = "Message from ". $from . " of the " . $institute . " institute : " . "\n\n" . $text;
    }else{
    	$message = "Message : " . "\n\n" . $text;
    }
    $message2 = "Here is a copy of your message: " . "\n\n" . $text;

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;

    mail($to,$subject,$message,$headers);
    mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender

    echo "Mail Sent. Thank you, we will contact you shortly.";

    header( "refresh:3;url=feedback.html" );


?>
