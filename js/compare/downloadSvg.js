window.onload = function(e){ 
function downloadImage(){
    var a = document.createElement('a');
    // toDataURL defaults to png, so we need to request a jpeg, then convert for file download.
    a.href = c.toDataURL("image/png");
    a.download = 'MitoXplorer.png';
    //a.click();

    // create a mouse event
    var event = new MouseEvent('click');

    // dispatching it will open a save as dialog in FF
    a.dispatchEvent(event);
}

var c = document.getElementById("canvasDownload");

function downloadSCplot(){

    browserZoomLevel = window.devicePixelRatio;
    document.getElementById('sidebar').parentNode.style.overflow = 'visible';


    html2canvas(document.querySelector("#sidebar"),{type:'view'}).then(sidebarImage => {
        
        //check the width and height of the canvas element
        svgsWidth = document.getElementById("scatterplotsvg").getBBox().width*1.5*browserZoomLevel + document.getElementById("barchartsvg").getBBox().width*1.5*browserZoomLevel + 20
        svgsHeight = document.getElementById("barchartsvg").getBBox().height*1.5*browserZoomLevel + document.getElementById("heatmapsvg").getBBox().height*1.5*browserZoomLevel + 50

        canvasWidth = sidebarImage.width + svgsWidth
        canvasHeight = sidebarImage.height > svgsHeight ? sidebarImage.height : svgsHeight

        //set the width and height accordingly
        c.width = canvasWidth
        c.height = canvasHeight

        //draw sidebar
        var ctx = c.getContext("2d");
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, c.width, c.height);
        ctx.drawImage(sidebarImage,0,0);

        //draw the svgs
        svgAsPngUri(document.getElementById("scatterplotsvg"), {scale: 1.5, backgroundColor: "white", canvg: window.canvg}, function(uri) {
            base_image1 = new Image();
            base_image1.src = uri;
            base_image1.onload = function(){
                ctx.drawImage(base_image1, sidebarImage.width+20, 50);
            }
        });

        svgAsPngUri(document.getElementById("barchartsvg"), {scale: 1.5, backgroundColor: "white", canvg: window.canvg}, function(uri) {
            base_image2_offsetWidth = document.getElementById("scatterplotsvg").getBBox().width*1.5*browserZoomLevel + sidebarImage.width;
            base_image2 = new Image();
            base_image2.src = uri;
            base_image2.onload = function(){
            ctx.drawImage(base_image2, base_image2_offsetWidth+40, 0);
            }
        });

        svgAsPngUri(document.getElementById("heatmapsvg"), {scale:1.5, backgroundColor: "white", canvg: window.canvg}, function(uri) {
            base_image3_offsetHeight = document.getElementById("barchartsvg").getBBox().height*1.5*browserZoomLevel
            base_image3 = new Image();
            base_image3.src = uri;
            base_image3.onload = function(){
                ctx.drawImage(base_image3, sidebarImage.width+20, base_image3_offsetHeight+30);
                downloadImage();
            }
        });
    });

}

function downloadPCA(){

    sidebarWidth = document.getElementById("sidebar").offsetWidth
    sidebarHeight = document.getElementById("sidebar").offsetHeight

    pcbarchartWidth = document.getElementById("pcbarchart").offsetWidth
    pcbarchartHeight = document.getElementById("pcbarchart").offsetHeight+document.getElementById("pcbcsvg").offsetWidth

    browserZoomLevel = window.devicePixelRatio;
    
    pcacanvas = document.getElementById("pcacanvas")
    pcacanvasWidth = pcacanvas.width*browserZoomLevel
    pcacanvasHeight = pcacanvas.height*browserZoomLevel

    //check the width and height of the canvas element

    canvasWidth = sidebarWidth + pcacanvasWidth + pcbarchartWidth
    canvasHeight = sidebarHeight > pcbarchartHeight ? (sidebarHeight > pcacanvasHeight ? sidebarHeight : pcacanvasHeight) : (pcbarchartHeight > pcacanvasHeight ? pcbarchartHeight : pcacanvasHeight)

    //set the width and height accordingly
    c.width = canvasWidth
    c.height = canvasHeight

    var ctx = c.getContext("2d");
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, c.width, c.height);

   $("#canvasDownloadPCA").css("display","block")
    html2canvas(document.querySelector("#canvasDownloadPCA")).then(pca => {
        ctx.drawImage(pca,sidebarWidth,0);
        $("#canvasDownloadPCA").css("display","none")
         
    });

    html2canvas(document.querySelector("#sidebar")).then(sidebarImage => {
        //draw sidebar
        ctx.drawImage(sidebarImage,0,0);
    });

    html2canvas(document.querySelector("#pcbarchart")).then(pcbctop => {
        ctx.drawImage(pcbctop,sidebarWidth + pcacanvasWidth,0);
         
    });

    $("#test2").css("display","block")
    html2canvas(document.querySelector("#test2")).then(pcbc => {
        ctx.drawImage(pcbc,sidebarWidth + pcacanvasWidth+20,document.getElementById("pcbarchart").offsetHeight);
        $("#test2").css("display","none")
        downloadImage()
         
    });

}

function downloadHeatmap(){

    sidebarWidth = document.getElementById("sidebar").offsetWidth
    sidebarHeight = document.getElementById("sidebar").offsetHeight

    processSelectorHeight = document.getElementById("processSelector").offsetHeight

    svgsWidth = document.getElementById("heatmap").offsetWidth
    svgsHeight = document.getElementById("heatmap").offsetHeight

    //check the width and height of the canvas element
    canvasWidth = sidebarWidth + svgsWidth +20
    canvasHeight = sidebarHeight > processSelectorHeight+svgsHeight+20 ? sidebarHeight+10 : processSelectorHeight+svgsHeight+30

    //set the width and height accordingly
    c.width = canvasWidth
    c.height = canvasHeight

    var ctx = c.getContext("2d");
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, c.width, c.height);

    html2canvas(document.querySelector("#sidebar"),{scale:1}).then(sidebarImage => {                    
        ctx.drawImage(sidebarImage,0,0);
    });

    html2canvas(document.querySelector("#processSelector"),{scale:1}).then(processSelector => {
        console.log(sidebarWidth)
        ctx.drawImage(processSelector,sidebarWidth,0);

        //draw the svgs
        svgAsPngUri(document.getElementById("clhmsvg"), {scale: 1, backgroundColor: "white", canvg: window.canvg}, function(uri) {
            base_image1 = new Image();
            base_image1.src = uri;
            base_image1.onload = function(){
                ctx.drawImage(base_image1, sidebarWidth+20, processSelectorHeight+30);
                downloadImage();
            }
        });
    });

}
$("#downloadsvg").click(function(){

    if ($("#svgs-all")[0].childNodes[0].id == "scatterplot") downloadSCplot();
    if ($("#svgs-all")[0].childNodes[0].id == "pca") downloadPCA();
    if ($("#svgs-all")[0].childNodes[0].id == "processSelector") downloadHeatmap()
})
}