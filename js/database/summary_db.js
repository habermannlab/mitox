function overview(){
    document.getElementById("beforedb").style.display = "";
    document.getElementById("interactome").style.display = "none";
    document.getElementById("loadingvis").style.display = "none";
    document.getElementById("visside").style.display = "none";
}

function interactome(organism){
    App.init(organism)
    document.getElementById("beforedb").style.display = "none";
    document.getElementById("interactome").style.display = "";
    document.getElementById("loadingvis").style.display = "";
    document.getElementById("visside").style.display = "";
}


function Piechart(){
  	var PC = {};

	var main_margin = {top: 10, right: 10, bottom: 30, left: 10},
	    main_width = 500 - main_margin.left - main_margin.right,
	    main_height = 500 - main_margin.top - main_margin.bottom,
	    radius = Math.min(main_width, main_height) / 2;

	var color = d3.scale.ordinal()
	    .range(["#8dd3c7", "#ffffb3", "#bebada", "#fb8072", "#80b1d3", "#fdb462", "#b3de69", "#fccde5", "#d9d9d9", "#bc80bd", "#ccebc5", "#f781bf", "#fbb4ae", "#b3cde3", "#ffed6f", "#decbe4", "#fed9a6"]);

	var arc = d3.svg.arc()
	    .outerRadius(radius - 10)
	    .innerRadius(0);

	var label = d3.svg.arc()
	    .outerRadius(radius - 100)
	    .innerRadius(radius - 100);

	var pie = d3.layout.pie()
	    .sort(null)
	   .startAngle(1.1*Math.PI)
	    .endAngle(3.1*Math.PI)
	    .value(function(d) { return d.total; });

	PC.init = function(divname,dataset){

	 	var div = d3.select("body").append("div").attr("class", "toolTip");

    var resp = d3.select(divname)
        .append('div')
        .attr('class', 'svg-container'); //container class to make it responsive
      
	    var svg = resp.append("svg")
	        .attr('class', 'canvas svg-content-responsive')
	        .attr('preserveAspectRatio', 'xMinYMin meet')
	        .attr('viewBox', [0, 0, main_width + main_margin.left + main_margin.right, main_height + main_margin.top + main_margin.bottom].join(' '))
	      .append("g")
	        .attr("transform", "translate(" + (main_margin.left + (main_width / 2))  + "," + (main_margin.top + (main_height / 2))  + ")");


	     var g = svg.selectAll(".arc")
	          .data(pie(dataset))
	        .enter().append("g")
	          .attr("class", "arc");

	      g.append("path")
	      .style("fill", function(d) { return color(d.data.name); })
	        .transition().duration(1500)
	      .attrTween('d', function(d) {
	        var i = d3.interpolate(d.startAngle+0.1, d.endAngle);
	        return function(t) {
	          d.endAngle = i(t); 
	          return arc(d)
	          }
	        }); 
	      g.append("text")
	          .attr("transform", function(d) { return "translate(" + label.centroid(d) + ")"; })
	          .attr("dy", ".35em")
	        .transition()
	        .delay(1500)
	          .text(function(d) { return d.data.name; })
	          .style('display', function (d) { return (d.endAngle-d.startAngle)/Math.PI*180 > 60 ? null : "none"; });

	      d3.selectAll("path").on("mousemove", function(d) {
	          div.style("left", d3.event.pageX+10+"px");
	          div.style("top", d3.event.pageY-25+"px");
	          div.style("display", "inline-block");
	        div.html((d.data.name)+"<br>"+(d.data.total));
	    });
	        
	    d3.selectAll("path").on("mouseout", function(d){
	        div.style("display", "none");
	    });
	    }
	    
	//d3.select("body").transition().style("background-color", "#d3d3d3");
	function type(d) {
	  d.total = +d.total;
	  return d;
	}
  	return PC
}

    PC1=Piechart();
    PC2=Piechart();
    PC3=Piechart();

var this_js_script = $('script[src*=summary]');
    var host = this_js_script.attr('host'),
        port = this_js_script.attr('port'),
        user = this_js_script.attr('user'),
        passwd = this_js_script.attr('passwd'),
        unix_socket = this_js_script.attr('unix_socket');

var parameter = 'host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket;

    jQuery.ajax({
        url: "./python/database/summary_db.py", 
        data: parameter,
        type: "POST",
        //dataType: "json",    
        success: function (jsondata) {
            
            d3.select('#loadingdb').remove();
            document.getElementById("pies").style.display = ""

            var orgs = JSON.parse(jsondata[0]["orgs"])
            PC1.init("#pie1",orgs)

            var proj = JSON.parse(jsondata[0]["proj_type"])
            PC2.init("#pie2",proj)

            var proj = JSON.parse(jsondata[0]["proj"])
            PC3.init("#pie3",proj)

            var total = d3.sum(orgs, function(d){return d.total;})
            var orgsNum = Object.keys(orgs).length

            document.getElementById("pcinfo").innerHTML = "Total number of organisms: " + orgsNum + "<br>" + "Total number of samples: " + total;

        },
        error: function(e){
            console.log(e);
        }
    });
