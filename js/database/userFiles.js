var this_js_script = $('script[src*=userFile]'),
    sessionid = this_js_script.attr('session-id'),
    host = this_js_script.attr('host'),
    port = this_js_script.attr('port'),
    user = this_js_script.attr('user'),
    passwd = this_js_script.attr('passwd'),
    unix_socket = this_js_script.attr('unix_socket'),
    organism;

function deleteSelected(project){
    var selects = $('#table').bootstrapTable('getSelections');
    sampleIDs = $.map(selects, function (row) {
        return row.sampleID;
    });
    $("#confirm-delete").modal()
    
    var parameter = 'project='+project + '&organism='+organism + '&sessionid='+sessionid + '&host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket + '&sampleID='+ JSON.stringify(sampleIDs);
    
    document.getElementById('deleteSamples').onclick = function() {
        
        jQuery.ajax({
            url: "./python/database/delete_table.py",
            data: parameter,
            type: "POST",
            error: function (e) {
                console.log(e);
            },
            success: function (response) {
                if (response[0] == "Success"){
                    $('#table').bootstrapTable('remove', {
                        field: 'sampleID',
                        values: sampleIDs
                    });
                }
            }
        });
        
    }
}

function getProjects(){

    var parameter = 'sessionid='+sessionid + '&host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket
    jQuery.ajax({
        url: "./python/database/get_projects.py",
        data: parameter,
        type: "POST",
        error: function (e) {
            console.log(e);
        },
        success: function (data) {

            var proj_type_list = []
            var proj_list = []

            jQuery.each(data, function(index, value){
                
                organism = value['organism'].split(" ").join("_")
                proj_type = value['proj_type'].split(" ").join("_")
                proj = value['folder'].split(" ").join("_")

                // Adding non-repetitive elements to project type
                if (proj_type_list.indexOf(organism+proj_type) < 0){
                    var li_element = document.createElement("li");
                    li_element.setAttribute("class", "list-group-item proj_type "+organism);
                    li_element.setAttribute("style", "display:none")
                    var a_element = document.createElement("a");
                    a_element.setAttribute("href", "#");
                    a_element.setAttribute("onclick", "return updateProj('"+organism+"','"+proj_type+"')");
                    a_element.innerHTML = value['proj_type']
                    li_element.appendChild(a_element)
                    document.getElementById("list_projtype").appendChild(li_element)
                    proj_type_list.push(organism+proj_type)
                }

                // // Adding non-repetitive elements to projects
                if (proj_list.indexOf(organism+proj) < 0){
                    var li_element = document.createElement("li");
                    li_element.setAttribute("class", "list-group-item proj "+organism+" "+proj_type);
                    li_element.setAttribute("style", "display:none")
                    var a_element = document.createElement("a");
                    a_element.setAttribute("href", "#");
                    a_element.setAttribute("onclick", "return createTable('"+value['folder']+"','"+value['organism']+"')");
                    a_element.innerHTML = value['folder']
                    li_element.appendChild(a_element)
                    document.getElementById("list_proj").appendChild(li_element)
                    proj_list.push(organism+proj)
                }
            })
        }
    });
}

function updateProjtype(organism){

    $('#collapse_projtype').collapse({toggle:false})
    //$('#collapse_projtype').collapse("hide")

    var all_pt= document.getElementsByClassName("proj_type");
    for (i = 0; i < all_pt.length; i++) {
        all_pt[i].style.display = "none"
    }

    var all_p = document.getElementsByClassName("proj");
    for (i = 0; i < all_p.length; i++) {
        all_p[i].style.display = "none"
    }

    var target = document.getElementsByClassName("proj_type "+organism)
    for (i = 0; i < target.length; i++) {
        target[i].style.display = ""
    }

    $('#collapse_projtype').collapse("show")

}

function updateProj(organism, proj_type){

    $('#collapse_proj').collapse({toggle:false})

    var all_p = document.getElementsByClassName("proj");
    for (i = 0; i < all_p.length; i++) {
        all_p[i].style.display = "none"
    }

    var target = document.getElementsByClassName("proj "+organism+" "+proj_type)
    for (i = 0; i < target.length; i++) {
        target[i].style.display = ""
    }

    $('#collapse_proj').collapse("show")
}

function createTable(project,organismInput){
    
    organism = organismInput
    
    document.getElementById("beforedb").style.display = "none";
    document.getElementById("db").style.display = "";
    
    var parameter = 'project='+project + '&organism='+organism + '&sessionid='+sessionid + '&host='+host + '&port='+port + '&user='+user + '&passwd='+passwd + '&unix_socket='+unix_socket
    var checkedRows = [];

    jQuery.getJSON("./main_files/project_description.json", function(data){
        jQuery.each(data, function(i, item){
            if (item['project'] == project && item['organism'] == organism){

                document.getElementById("database-head").innerHTML = (item['title'] != "") ? item['title'] : project
                
                if (item['ref'] != "") {
                    document.getElementById("database-ref").style.display = "";
                    document.getElementById("database-ref").innerHTML = item['ref'];
                    if (item['url'] != "") document.getElementById("database-ref").innerHTML += "<br>(<a href='" + item['url'] + "'>" + item['url'] + "</a>)";
                }else{
                    document.getElementById("database-ref").style.display = "none";
                }

            }
        })
    })
       
    console.log(parameter) 

    $( "#loading" ).css('display','');
    document.getElementById("userfiles").innerHTML=""
  
    $("#deleteSelected").remove();
    
    if (project == "User_upload"){
        var childNode = document.createElement("button");
        childNode.className = "btn btn-danger";
        childNode.id = "deleteSelected";
        childNode.innerHTML = "Delete"
        childNode.setAttribute("onClick", "deleteSelected('"+project+"')")
        document.getElementById("buttons").appendChild(childNode); 
    }
 

    
    var table = document.createElement("TABLE");
    table.setAttribute("id", "table");
    table.setAttribute("style", "font-size:14px");
    document.getElementById("userfiles").appendChild(table)
    
    jQuery.ajax({
        url: "./python/database/create_table.py", 
        data: parameter,
        type: "POST",
        //dataType: "json",    
        success: function (jsonData) {
            $( "#loading" ).css('display','none');
            if(typeof jsonData != 'object'){
                checkedRows = [];
                message = "Error connecting to the database, please try again later!";
                if (project == "User_upload") message = "It appears that you haven't uploaded anything yet!"
                document.getElementById("userfiles").innerHTML=""
                var div = document.createElement("div");
                div.setAttribute("class", "col-md-12");
                div.setAttribute("style", "font-size:14px");
                div.innerHTML="<br><br><b>"+message+"</b>"
                document.getElementById("userfiles").append(div)
            }else{
                checkedRows = [];
                var column = [{
                        field: 'state',
                        checkbox: true
                        },{
                        field: 'sampleID',
                        title: 'Sample ID',
                        sortable: true,
                        formatter: function sampleFormatter(value){
                            return '<a href="mitomodel.php?sampleID='+value+'&organism='+organism+'&sessionid='+sessionid+'" target="_blank"><span class="glyeye glyphicon glyphicon-eye-open" aria-hidden="true" style="font-size: 1.2em;color:black;padding-right:10px"></span></a>'+value
                        },
                        cellStyle: function cellStyle(){
                            return {
                            css: {"white-space": "nowrap"}
                          };
                        }
                        }];
                keys = Object.keys(jsonData[0])
                for (var i = 0; i < keys.length; i++){
                    var entry = new Object();
                    entry.field = keys[i];
                    entry.title = keys[i].charAt(0).toUpperCase() + keys[i].slice(1);
                    entry.sortable = true
                    if (keys[i] != "sampleID" && keys[i] != "cancer_type" && keys[i] != "flySex") column.push(entry);
                }
                $('#table').bootstrapTable({
                    search: true,
                    pagination: true,
                    columns: column,
                    data: jsonData,
                    clickToSelect: true
                });

                $('#table').on('check.bs.table', function (e, row) {
                    //console.log(row.sampleID)
                  checkedRows.push(row.sampleID);
                });

                $('#table').on('uncheck.bs.table', function (e, row) {
                  $.each(checkedRows, function(index, value) {
                    //console.log(row.sampleID)
                      if (value === row.sampleID) checkedRows.splice(index,1)
                  });
                });
                
                $('#table').on('check-all.bs.table', function (e,rows) {
                    checkedRows = []
                    $.each(rows, function(index,value){
                        //console.log(value.sampleID)
                        checkedRows.push(value.sampleID)
                    })
                });
                
                $('#table').on('uncheck-all.bs.table', function (e,rows) {checkedRows = []});
            }
            
        },error: function(e){
            console.log(e);
        }})

    $("#readygo").click(function() {
        //console.log(checkedRows)
        if (checkedRows.length == 0){
           onError(new Error('Please select samples!!'));
        }
        if (checkedRows.length > 6){
            onError(new Error('Please select 6 samples at most!!'));
        } 
        
        var phptext ='compare.php?organism='+organism
        $.each(checkedRows, function(index, value) {
          phptext = phptext + '&compare%5B%5D='+value+'&'
      });
        window.location.href = phptext;
    });

    
    $(".dismiss").click(function(e){
        $('input:checkbox').removeAttr('checked');
        $(".database").css('background-color', 'white');
    })
    
    $(".close-modal").click(function(e){
        $('input:checkbox').removeAttr('checked');
        $(".database").css('background-color', 'white');
    })
    
    function onError(res) {
    document.getElementById('warning').innerHTML="<font color=\"red\">"+res;
    $( "#warning" ).fadeIn( 300 ).delay( 400 ).fadeOut( 300 );
    throw new Error("Something went badly wrong!");
    }
    
    
}

getProjects()
