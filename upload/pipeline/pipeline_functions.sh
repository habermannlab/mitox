#!/bin/bash
AddPath (){

	##################################################
	### Add the links to the PATH
	### Checking if all softwares are provided in the PATH
	##################################################

	software_paths=$path_fastqc':'$path_trimgalore':'$path_cutadapt':'$path_cufflinks':'$path_STAR
	PATH=$software_paths:$PATH

	software=(fastqc trim_galore cutadapt cufflinks STAR $software_picard $software_picard)
	software_links=("https://www.bioinformatics.babraham.ac.uk/projects/download.html#fastqc" "https://www.bioinformatics.babraham.ac.uk/projects/download.html#trim_galore" "http://cutadapt.readthedocs.io/en/stable/guide.html" "http://cole-trapnell-lab.github.io/cufflinks/install/" "https://github.com/alexdobin/STAR/releases" "https://broadinstitute.github.io/picard/" "https://software.broadinstitute.org/gatk/download/archive")
	installed=true

	for (( i=0; i < 7; ++i ))
	do
		if ! type "${software[$i]}" 2> /dev/null && [ ! -f "${software[$i]}" ]; then
		    echo "*****************************************"
		    echo "${software[$i]} is not installed or not provided in the PATH"
		    echo "Please provide the path to the software within the script or download it from:"
		    echo "${software_links[$i]}"
		    echo "*****************************************"
		    echo ""
		    installed=false	
		else
			echo "${software[$i]} is installed"
			echo ""
		fi
	done

	if ! $installed ; then
	    exit 1
	fi

}

FileCheck(){

	#################################################
	### Check if the paths to the files are valid
	#################################################

	ref_genome_dir=$(dirname "${ref_genome}")
	num_normal=${#sample_normal[@]}
	num_disease=${#sample_disease[@]}

	if [ ${ref_genome: -3} != ".fa" && ${ref_genome: -6} != ".fasta"] || [ ! -f "$ref_genome" ]; then
		echo "This is not a .fa/.fasta file or the file does not exist, or an indexed file does not exist in the same directory as the .fa file"	
		exit 1
	fi

	count=`ls -1 $ref_genome_dir/*.fai 2>/dev/null | wc -l`
	if [ $count -eq 0 ]; then
		echo "Index of the genome does not exist (index the genome with samtools: samtools faidx <name of your reference genome>)"	
		exit 1
	fi

	count=`ls -1 $ref_genome_dir/*.dict 2>/dev/null | wc -l`
	if [ $count -eq 0 ]; then
		echo "Dictionary of the genome does not exist (create a dictionary with picard CreateSequenceDictionary: java -jar <path to picard software> CreateSequenceDictionary.jar R=<name of reference genome> O=<name of output dict>)"	
		exit 1
	fi

	if [ ${knownSitesRealigner: -4} != ".vcf" ] || [ ! -f "$knownSitesRealigner" ]; then
		echo "The known site file for Realigner is not a vcf file or the file does not exist. Check the Readme file for details"
		exit 1
	fi

	if [ ${knownSitesRecal: -4} != ".vcf" ] || [ ! -f "$knownSitesRecal" ]; then
		echo "The known site file for Recalibration is not a vcf file or the file does not exist. Check the Readme file for details"
		exit 1
	fi

	if [ ${knownSitesCaller: -4} != ".vcf" ] || [ ! -f "$knownSitesCaller" ]; then
		echo "The known site file for Mutation calling is not a vcf file or the file does not exist. Check the Readme file for details"
		exit 1
	fi

	if [ ${gtf_file: -4} != ".gtf" ] || [ ! -f "$gtf_file" ]; then
		echo "The GTF file is not with a gtf extension or the file does not exist."
		exit 1
	fi

	if [ $dir_output = "" ]; then
		echo "The directory for output is empty"
		exit 1
	fi

	if [ $num_normal -lt 1 ]; then
		echo "The number of normal samples is less than 1"
		exit 1
	fi

	for (( i=0; i < $num_normal; ++i ))
	do
	    ((num=$i+1))
		count=`ls -1 ${sample_normal[$i]}/*.fastq 2>/dev/null | wc -l`
		if [ $count -lt 1 ]; then
			echo "Fasta/fastq files do not exist within the directory of normal sample replicate $num."
			echo "${sample_normal[$i]}"
			exit 1
		fi
	done

	if [ $num_disease -lt 1 ]; then
		echo "The number of disease samples is less than 1"
		exit 1
	fi

	for (( i=0; i < $num_disease; ++i ))
	do
	    ((num=$i+1))
		count=`ls -1 ${sample_disease[$i]}/*.fastq 2>/dev/null | wc -l`
		if [ $count -lt 1 ]; then
			echo "Fasta/fastq files do not exist within the directory of disease sample replicate $num."
			echo "${sample_disease[$i]}"
			exit 1
		fi
	done

	case $sample_read in
        1) ;;
        2) ;;
        *) 
			echo "The sample_read has to be either 1 for single-end or 2 for paired-end samples" 
			exit 1
			;;
    esac

}

CreateOutputDir(){

	#Check if the path to the user-designated output directory is valid
	#if not, create one
	if [ ! -d "$dir_output" ]; then
		echo  "The output directory does not exist. It will be created..."
		mkdir -p $dir_output
		if [ "$?" -ne "0" ]; then
		  echo "Failed to create output directory"
		  exit 1
		fi
	fi

	mkdir -p $dir_output/files
	dir_output_files="$dir_output/files"

	sample_name=()
	sample_type=()
	dir_files=()

	#Create directories to store the output for each replicate of normal and disease sample
	for (( i=0; i < $num_normal; ++i ))
	do
		mkdir -p $dir_output_files/normal_$i
		sample_name+=(${sample_normal[$i]})
		sample_type+=("normal")
		dir_files+=($dir_output_files/normal_$i)

		if [ "$?" -ne "0" ]; then
		  echo "Failed to create output directory for sample:"
		  echo "${sample_normal[$i]}"
		  exit 1
		fi
	done

	for (( i=0; i < $num_disease; ++i ))
	do
		mkdir -p $dir_output_files/disease_$i
		sample_name+=(${sample_disease[$i]})
		sample_type+=("normal")
		dir_files+=($dir_output_files/disease_$i)

		if [ "$?" -ne "0" ]; then
		  echo "Failed to create output directory for sample"
		  echo "${sample_disease[$i]}"
		  exit 1
		fi
	done

	num_sample=${#sample_type[@]}
		
}

QualityControl(){

	# For each replicates of normal sample do the following QC and trimming steps
	for (( i=0; i < $num_sample; ++i ))
	do
		echo "============================================================="
		echo "Start quality control for normal sample with FastQC..."
		echo "${sample_name[$i]}"
		echo "============================================================="

		mkdir -p ${dir_files[$i]}/fastqc
		mkdir -p ${dir_files[$i]}/trimgalore

		# If the sample is single-end
		if [ $sample_read -eq 1 ]; then

			fastqc -o ${dir_files[$i]}/fastqc ${sample_name[$i]}/*.fastq;
			
			echo "============================================================="
			echo "Finish FastQC."
			echo "============================================================="
			echo "============================================================="
			echo "Filtering low quality reads and removing adaptor with Trim Galore..."
			echo "============================================================="
			
			trim_galore -q 20 --fastqc -o ${dir_files[$i]}/trimgalore ${sample_name[$i]}/*.fastq;
			
		else
			# If the sample is paired-end

			fastqc -o ${dir_files[$i]}/fastqc ${sample_name[$i]}/*_1.fastq ${sample_name[$i]}/*_2.fastq;
			
			echo "============================================================="
			echo "Finish FastQC."
			echo "============================================================="
			echo "============================================================="
			echo "Filtering low quality reads and removing adaptor with Trim Galore..."
			echo "============================================================="
			
			trim_galore -q 20 --fastqc -o ${dir_files[$i]}/trimgalore --paired ${sample_name[$i]}/*_1.fastq ${sample_name[$i]}/*_2.fastq;

		fi

		echo "============================================================="
		echo "Finish filtering low quality reads."
		echo "============================================================="

		echo "Results of FastQC before filtering: ${dir_files[$i]}/fastqc"
		echo "Results of FastQC before filtering: ${dir_files[$i]}/trimgalore"

	done

}

Mapping(){
	# For each replicates of normal sample do the following Mapping step with STAR
	for (( i=0; i<$num_sample; ++i ))
	do
		echo "============================================================="
		echo "Mapping to the genome with STAR aligner..."
		echo "${sample_name[$i]}"
		echo "============================================================="

		mkdir -p ${dir_files[$i]}/STAR

		# If the sample is single-end
		if [ $sample_read -eq 1 ]; then

			STAR --runThreadN 10 --genomeDir $ref_genome_dir --readFilesIn ${dir_files[$i]}/trimgalore/*trimmed.fq --outFileNamePrefix ${dir_files[$i]}/STAR/ --sjdbGTFfile $gtf_file --outSAMstrandField intronMotif --outSAMtype BAM SortedByCoordinate --outReadsUnmapped Fastx;
			
		else
			# If the sample is paired-end

			STAR --runThreadN 10 --genomeDir $ref_genome_dir --readFilesIn ${dir_files[$i]}/trimgalore/*_1.fq ${dir_files[$i]}/trimgalore/*_2.fq --outFileNamePrefix ${dir_files[$i]}/STAR/ --sjdbGTFfile $gtf_file --outSAMstrandField intronMotif --outSAMtype BAM SortedByCoordinate --outReadsUnmapped Fastx;
		fi

		echo "============================================================="
		echo "Finish mapping."
		echo "Showing a summary of mapping..."
		echo "============================================================="

		cat ${dir_files[$i]}/STAR/*.final.out

	done

}

DiffExp(){

	cuffdiff_path=""

	for (( i=0; i < $num_normal; ++i ))
	do
		dir_files="$dir_output_files/normal_$i"
		mkdir -p $dir_files/cufflinks
		
		echo "============================================================="
		echo "Assemblying transcripts with cufflinks..."
		echo "${sample_normal[$i]}"
		echo "============================================================="
		
		cufflinks -G $gtf_file -o $dir_files/cufflinks $dir_files/STAR/*.out.bam
		
		echo $dir_files/cufflinks/transcripts.gtf >> $dir_output_files/assemblies.txt

		if (( $i != $num_normal-1 )); then
			cuffdiff_path="$cuffdiff_path$dir_files/STAR/Aligned.sortedByCoord.out.bam,"
		else
			cuffdiff_path="$cuffdiff_path$dir_files/STAR/Aligned.sortedByCoord.out.bam "
		fi

		echo "============================================================="
		echo "Finish Assemblying"
		echo "============================================================="

	done

	for (( i=0; i < $num_disease; ++i ))
	do
		dir_files="$dir_output_files/disease_$i"
		mkdir -p $dir_files/cufflinks

		echo "============================================================="
		echo "Assemblying transcripts..."
		echo "${sample_disease[$i]}"
		echo "============================================================="
		
		cufflinks -G $gtf_file -o $dir_files/cufflinks $dir_files/STAR/*.out.bam
		
		echo $dir_files/cufflinks/transcripts.gtf >> $dir_output_files/assemblies.txt

		if (( $i != $num_disease-1 )); then
			cuffdiff_path="$cuffdiff_path$dir_files/STAR/Aligned.sortedByCoord.out.bam,"
		else
			cuffdiff_path="$cuffdiff_path$dir_files/STAR/Aligned.sortedByCoord.out.bam "
		fi

		echo "============================================================="
		echo "Finish Assemblying"
		echo "============================================================="

	done

	echo "============================================================="
	echo "Merging all transcripts"
	echo "============================================================="

	# cuffmerge -o $dir_output_files/merged_asm -g $gtf_file -s $ref_genome $dir_output_files/assemblies.txt

	echo "============================================================="
	echo "Testing for differential expression..."
	echo "============================================================="

	cuffdiff -o $dir_output_files/cuffdiff/ -b $ref_genome -L normal,disease -u $dir_output_files/merged_asm/merged.gtf $cuffdiff_path

	echo "============================================================="
	echo "Testing for differential expression of genes from mitochondrial genome..."
	echo "============================================================="

	mkdir -p $dir_output_files/cuffdiff_chrM

	grep chrM $gtf_file > $dir_output_files/cuffdiff_chrM/chrM.gtf

	cuffdiff -o $dir_output_files/cuffdiff_chrM/ -b $ref_genome -L normal,disease -u $dir_output_files/cuffdiff_chrM/chrM.gtf $cuffdiff_path

	echo "============================================================="
	echo "Finish Differential expression analysis"
	echo "============================================================="

}

Mutation(){

	##################################################
	### Mutation analysis
	### Process bam files and generate vcf for normal samples
	##################################################

	for (( i=0; i<$num_sample; ++i ))
	do
		mkdir -p ${dir_files[$i]}/mutation

		echo "============================================================="
		echo "Starting the Mutation pipeline for sample..."
		echo "${sample_normal[$i]}"
		echo "============================================================="
		echo "============================================================="
		echo "Add or Replace Read groups with Picard..."
		echo "============================================================="

		# ILLUMINA, SOLID, LS454, HELICOS and PACBIO
		java -jar $software_picard AddOrReplaceReadGroups I=${dir_files[$i]}/STAR/Aligned.sortedByCoord.out.bam O=${dir_files[$i]}/mutation/Aligned_readgroup.bam SO=coordinate RGID=NORMAL RGLB=library RGPL=illumina RGPU=machine RGSM=normal_$i
		
		echo "============================================================="
		echo "Mark duplicates with Picard..."
		echo "============================================================="
		
		java -jar $software_picard MarkDuplicates I=${dir_files[$i]}/mutation/Aligned_readgroup.bam O=${dir_files[$i]}/mutation/Aligned_deduped.bam M=${dir_files[$i]}/mutation/output.metrics

		echo "============================================================="
		echo "Reorder bam file with Picard..."
		echo "============================================================="
		
		java -jar $software_picard ReorderSam I=${dir_files[$i]}/mutation/Aligned_deduped.bam O=${dir_files[$i]}/mutation/Aligned_reorder.bam R=$ref_genome CREATE_INDEX=TRUE
		
		echo "============================================================="
		echo "Split CIGAR reads with GATK..."
		echo "============================================================="

		# Reassign mapping quality since STAR assign quality of 255 to uniquely mapped gene, which would be filtered by GATK
	    java -jar $software_gatk -T SplitNCigarReads -R $ref_genome -I ${dir_files[$i]}/mutation/Aligned_reorder.bam -o ${dir_files[$i]}/mutation/Aligned_split.bam -rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_N_CIGAR_READS

		echo "============================================================="
		echo "Realign Target Creator with GATK..."
		echo "============================================================="

		# -drf DuplicateRead tells GATK to ignore duplicate reads
	    java -jar $software_gatk -T RealignerTargetCreator -R $ref_genome -I ${dir_files[$i]}/mutation/Aligned_split.bam -known $knownSitesRealigner -o ${dir_files[$i]}/mutation/output_split.intervals -drf DuplicateRead

	    echo "============================================================="
		echo "Realign indels with GATK..."
		echo "============================================================="

	    java -jar $software_gatk -T IndelRealigner -R $ref_genome -I ${dir_files[$i]}/mutation/Aligned_split.bam -known $knownSitesRealigner -targetIntervals ${dir_files[$i]}/mutation/output_split.intervals -o ${dir_files[$i]}/mutation/Aligned_realigned.bam -drf DuplicateRead

	    echo "============================================================="
		echo "Recalibrating base with GATK..."
		echo "============================================================="

		java -jar $software_gatk -T BaseRecalibrator -I ${dir_files[$i]}/mutation/Aligned_realigned.bam -R $ref_genome -knownSites $knownSitesRecal -o ${dir_files[$i]}/mutation/recall_data.table -drf DuplicateRead

	   	echo "============================================================="
		echo "Printing out reads with GATK..."
		echo "============================================================="

	    java -jar $software_gatk -T PrintReads -R $ref_genome -I ${dir_files[$i]}/mutation/Aligned_realigned.bam -BQSR ${dir_files[$i]}/mutation/recall_data.table -o ${dir_files[$i]}/mutation/Aligned_recalibrated.bam -drf DuplicateRead

	    echo "============================================================="
		echo "Calling haplotype variants with GATK..."
		echo "============================================================="

	    java -jar $software_gatk -T HaplotypeCaller -R $ref_genome -I ${dir_files[$i]}/mutation/Aligned_recalibrated.bam -dontUseSoftClippedBases -stand_call_conf 20 -out_mode EMIT_VARIANTS_ONLY -o ${dir_files[$i]}/mutation/output.vcf --dbsnp $knownSitesCaller -drf DuplicateRead

	done

}