#!/usr/bin/python

import cgi, os, re, sys
import cgitb;cgitb.enable()
import pandas as pd
import numpy as np
import pymysql
import json
from os import listdir
from os.path import isfile, join
from datetime import datetime

# tmp = [1,2,3,4,5,6,"N","n","n",1,2,3,4,5,6,7,8,"n","n","n"]

# userID = "testannie"
# organism = "Human"
# exppath = "test_exp/"
# mutpath = "test_mut/"

# mysql = json.load(open("../mysql/mysql_info.json"))
# host = mysql['host']
# port = mysql['port']
# user = mysql['user']
# passwd = mysql['passwd']
# unix_socket = mysql['unix_socket']

tmp = sys.argv[1:21]

userID = sys.argv[21]
organism = sys.argv[22]
exppath = sys.argv[23]
mutpath = sys.argv[24]

host = sys.argv[25]
port = sys.argv[26]
user = sys.argv[27]
unix_socket = sys.argv[28]
try: 
    passwd = sys.argv[29]
except IndexError:
    passwd = ''

error_log = []
expcol = ['sampleID','geneID','normal','abnormal','log2','pvalue','geneID_a1','geneID_a2','geneID_a3']
mutcol = ['sampleID','geneID','chrom','pos','ref','alt','eff','consq','geneID_a1','geneID_a2','geneID_a3']

def IsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

expdict = {}
counter = 0
for col in expcol:
    if IsInt(tmp[counter]): expdict[col] = int(tmp[counter]) - 1
    counter += 1
mutdict = {}
for col in mutcol:
    if IsInt(tmp[counter]): mutdict[col] = int(tmp[counter]) - 1
    counter += 1

##connecting to mysql database
try:
    conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=organism, unix_socket=unix_socket, local_infile=True)
    cur = conn.cursor()
    query = 'SELECT sampleID from file_directory WHERE userID = "'+userID+'"'
    file_directory = pd.read_sql(query, con=conn)
    mysample = file_directory['sampleID'].unique()
    query = 'SELECT geneID, geneID_obsolete, geneID_a1, geneID_a2, geneID_a3 from target'
    target = pd.read_sql(query, con=conn)
except:
    error_log.append("Error occur while connecting to the database")


#Deal with expression file
target_exp = pd.DataFrame()
exp_all = pd.DataFrame()

exp_exist=False
if os.path.isfile(exppath+"target_exp.csv"):
    os.unlink(exppath+"target_exp.csv")
if os.path.isfile(exppath+"all_exp.csv"):
    os.unlink(exppath+"all_exp.csv")
if os.path.isfile(exppath+"file_directory.csv"):
    os.unlink(exppath+"file_directory.csv")
    
for f in listdir(exppath):
    if (isfile(join(exppath,f)) and not f.startswith('.')):
        exp_exist = True
        exp = pd.DataFrame()
        try:
            thisfile = join(exppath,f)
            
            colnames=[]
            cols=[]
            for k, v in expdict.items():
                colnames.append(k)
                cols.append(v)
                
            exp = pd.read_csv(thisfile,sep='[,;\t]',header=0).iloc[:,cols]
            exp.columns = colnames
            
            for c in ['normal','abnormal']:
                if not(c in exp.columns): exp[c] = 0
                exp[c].fillna(0, inplace=True)
                
            if not('pvalue' in exp.columns): exp['pvalue'] = 1
            exp['pvalue'].fillna(1, inplace=True)
            
            for c in ['geneID','geneID_a1','geneID_a2','geneID_a3']:
                if not(c in exp.columns):
                    exp[c] = ""
                exp[c].fillna("", inplace=True)
                
            exp['userID'] = userID
            if not("sampleID" in exp.columns): 
                if f.endswith(".csv"):
                    exp['sampleID'] = f.split(".csv")[0] + "_user"
                elif f.endswith(".txt"):
                    exp['sampleID'] = f.split(".txt")[0] + "_user"
            else: exp['sampleID'] = exp['sampleID'] + "_user"
                
            exp = exp[["userID","sampleID","geneID","normal","abnormal","log2","pvalue","geneID_a1","geneID_a2","geneID_a3"]]
            
            #column checking
            if not (np.issubdtype(exp['log2'].dtype, np.number)):
                error_log.append("The file " + f + " has non-numerical Log2 Fold change value")
                break
            if not (np.issubdtype(exp['normal'].dtype, np.number)):
                error_log.append("The file " + f + " has non-numerical expression value for normal sample")
                break
            if not (np.issubdtype(exp['abnormal'].dtype, np.number)):
                error_log.append("The file " + f + " has non-numerical expression value for mutant sample")
                break
            if not (exp['pvalue'].between(0, 1).all()):
                error_log.append("The file " + f + " has p-value that is not between 0 and 1")
                break
                    
            exp_all = pd.concat([exp_all,exp])
                
        except pd.errors.EmptyDataError:
            pass
        except ValueError:
            error_log.append("The column numbers are entered incorrectly")
            break
        except:
            error_log.append("Error occurred while reading expression files")
            break

if not exp_exist:
    error_log.append("Expression files are not uploaded")

try:
    #merge with target genes, using all possible geneIDs
    exp_tmp = exp_all.copy()
    for IDtype in ['geneID','geneID_a1','geneID_a2','geneID_a3']:
        if IDtype in colnames:
            match = exp_tmp[exp_tmp[IDtype].isin(target[IDtype])]
            if IDtype != "geneID":
                match = pd.merge(match,target[["geneID",IDtype]],on=IDtype)
                match['geneID'] = match['geneID_y']
                match.drop(['geneID_x','geneID_y'],axis=1,inplace=True)
                match = match[['userID','sampleID','geneID','normal','abnormal','log2','pvalue','geneID_a1','geneID_a2','geneID_a3']]
            target_exp = pd.concat([target_exp,match])
            exp_tmp = exp_tmp[~exp_tmp[IDtype].isin(target[IDtype])]

    #Deal with obsolete ID
    match = exp_tmp[exp_tmp["geneID"].isin(target["geneID_obsolete"])]
    match.rename(columns={"geneID" : "geneID_obsolete"}, inplace=True)
    match = pd.merge(match,target[["geneID","geneID_obsolete"]],on="geneID_obsolete")
    match = match[['userID','sampleID','geneID','normal','abnormal','log2','pvalue','geneID_a1','geneID_a2','geneID_a3']]
    target_exp = pd.concat([target_exp,match])
            
    target_exp.drop_duplicates(subset=['sampleID','geneID'],keep='first',inplace=True)
    target_exp.dropna(subset=['sampleID','geneID'],inplace=True)
    target_exp.to_csv(exppath+"target_exp.csv",index=False)
            
    exp_all.drop_duplicates(subset=['sampleID','geneID'],keep='first',inplace=True)
    exp_all.dropna(subset=['sampleID','geneID'],inplace=True)
    exp_all.to_csv(exppath+"all_exp.csv",index=False)
    
    exp_sample = exp_all['sampleID'].unique()
    if (np.any(np.in1d(exp_sample, mysample))):
        error_log.append("There are duplicated samples in your database!")
except KeyError:
    if (error_log == []):
        error_log.append("Error occurred while reading expression files")
except:
    error_log.append("Something went wrong with the upload of expression file!")


####################################################################

#Deal with mutation file
target_mut = pd.DataFrame()
mut_all = pd.DataFrame()

mut_exist = False
if os.path.isfile(mutpath+"target_mut.csv"):
    os.unlink(mutpath+"target_mut.csv")
if os.path.isfile(mutpath+"all_mut.csv"):
    os.unlink(mutpath+"all_mut.csv")

for f in listdir(mutpath):
    if (isfile(join(mutpath,f)) and not f.startswith('.')):
        mut = pd.DataFrame()
        mut_exist = True
        try:
            thisfile = join(mutpath,f)
            
            colnames=[]
            cols=[]
            for k, v in mutdict.items():
                colnames.append(k)
                cols.append(v)
                
            mut = pd.read_csv(thisfile,sep='[,;\t]',header=0).iloc[:,cols]
            mut.columns = colnames
            
            for c in ['eff','consq','geneID','geneID_a1','geneID_a2','geneID_a3']:
                if not(c in mut.columns):
                    mut[c] = ""
                mut[c].fillna("", inplace=True)
                
            mut['userID'] = userID
            if not("sampleID" in mut.columns): 
                if f.endswith(".csv"): 
                    mut['sampleID'] = f.split(".csv")[0] + "_user"
                elif f.endswith(".txt"):
                    mut['sampleID'] = f.split(".txt")[0] + "_user"
            else: mut['sampleID'] = mut['sampleID'] + "_user"    
            
            mut = mut[['userID','sampleID','geneID','chrom','pos','ref','alt','eff','consq','geneID_a1','geneID_a2','geneID_a3']]         
            mut_all = pd.concat([mut_all,mut])
                
        except pd.errors.EmptyDataError:
            pass
        except ValueError:
            error_log.append("The column numbers are entered incorrectly")
            break
        except:
            error_log.append("Error occurred while reading mutation files")
            break

if mut_exist:
    try:
            #merge with target genes, using all possible geneIDs
        mut_tmp = mut_all.copy()
        for IDtype in ['geneID','geneID_a1','geneID_a2','geneID_a3']:
            if IDtype in colnames:
                match = mut_tmp[mut_tmp[IDtype].isin(target[IDtype])]
                if IDtype != "geneID":
                    match = pd.merge(match,target[["geneID",IDtype]],on=IDtype)
                    match['geneID'] = match['geneID_y']
                    match.drop(['geneID_x','geneID_y'],axis=1,inplace=True)
                    match = match[['userID','sampleID','geneID','chrom','pos','ref','alt','eff','consq','geneID_a1','geneID_a2','geneID_a3']]
                target_mut = pd.concat([target_mut,match])
                mut_tmp = mut_tmp[~mut_tmp[IDtype].isin(target[IDtype])]

        # Deal with obsolete ID
        match = mut_tmp[mut_tmp["geneID"].isin(target["geneID_obsolete"])]
        match.rename(columns={"geneID" : "geneID_obsolete"}, inplace=True)
        match = pd.merge(match,target[["geneID","geneID_obsolete"]],on="geneID_obsolete")
        match = match[['userID','sampleID','geneID','chrom','pos','ref','alt','eff','consq','geneID_a1','geneID_a2','geneID_a3']]
        target_mut = pd.concat([target_mut,match])

        target_mut['mutation'] = target_mut['chrom'].astype(str)+ ' ' + target_mut['pos'].astype(str) + ' ' + target_mut['ref'] + '|' + target_mut['alt'] + ': ' + target_mut['eff'] + ': ' + target_mut['consq']
        target_mut = target_mut[['userID','sampleID','geneID','mutation']]
        target_mut = target_mut.groupby(['userID','sampleID','geneID']).apply(lambda x: '; '.join(x.mutation)).reset_index()
        target_mut.columns = ['userID','sampleID','geneID','mutation']

        target_mut.drop_duplicates(subset=['sampleID','geneID'],keep='first',inplace=True)
        target_mut.dropna(subset=['sampleID','geneID'],inplace=True)
        target_mut.to_csv(mutpath+"target_mut.csv",index=False)

        mut_all.drop_duplicates(subset=['sampleID','geneID'],inplace=True)
        mut_all.dropna(axis=0,how='any',inplace=True)
        mut_all.to_csv(mutpath+"all_mut.csv",index=False)

    except KeyError:
        error_log.append("Error occurred while reading mutation files")
    except:
        error_log.append("Something went wrong with the upload of mutation file!")

####################################################################
#Create file directory file

if (error_log == []):
    try:
        my_file_directory = pd.DataFrame()
        my_file_directory['sampleID'] = exp_all['sampleID'].unique()
        my_file_directory['userID'] = userID
        my_file_directory['folder'] = "User_upload"
        my_file_directory['subfolder'] = ""
        my_file_directory['organism'] = organism
        my_file_directory['datetime'] = datetime.today().date().strftime('%Y%m%d')
        my_file_directory = my_file_directory[['userID','sampleID','folder','subfolder','organism','datetime']]
        my_file_directory.to_csv(exppath+"file_directory.csv",index=False)
        query = "LOAD DATA LOCAL INFILE '"+exppath+"file_directory.csv"+"' IGNORE INTO TABLE file_directory FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
        cur.execute(query)
        conn.commit()
    except:
        error_log.append("Something went wrong!")


####################################################################
#Upload files
if (error_log == []):
    try:  
        query = "LOAD DATA LOCAL INFILE '"+exppath+"all_exp.csv"+"' IGNORE INTO TABLE expression FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
        cur.execute(query)
        query = "LOAD DATA LOCAL INFILE '"+exppath+"target_exp.csv"+"' IGNORE INTO TABLE target_exp FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
        cur.execute(query)
        conn.commit()

        if mut_exist:
            query = "LOAD DATA LOCAL INFILE '"+mutpath+"all_mut.csv"+"' IGNORE INTO TABLE mutation FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
            cur.execute(query)
            query = "LOAD DATA LOCAL INFILE '"+mutpath+"target_mut.csv"+"' IGNORE INTO TABLE target_mut FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
            cur.execute(query)
            conn.commit()   
    except:
        error_log.append("Error occured while uploading to database!")
        # query = 'DELETE from file_directory WHERE sampleID in ('+','.join(exp_all['sampleID'].unique())+') AND userID ="'+sessionid+'"'
        # cur.execute(query)
        # conn.commit()


# Delete entries from MySQL if error exists
    
conn.close()

if (len(error_log) == 0):
    result = json.dumps(["No Error"])
else: 
    result = json.dumps(error_log)

# print ("Content-type: text/html\n")
# print ("<html>")
# print (result)
# print ("</html>")
print (result)

