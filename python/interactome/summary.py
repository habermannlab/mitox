#!/usr/bin/env python

import cgi
import cgitb;cgitb.enable()
import json
import pandas as pd
import pymysql
#import simplejson

form = cgi.FieldStorage()
host = form.getvalue('host')
port = form.getvalue('port')
user = form.getvalue('user')
passwd = form.getvalue('passwd')
unix_socket = form.getvalue('unix_socket')

# sessionid = 'test'
# host = "localhost"
# port = 3306
# user = "root"
# passwd = ""
# unix_socket = "/tmp/mysql.sock"

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Human", unix_socket=unix_socket)

query1 = "select geneID, gene_name, process, gene_function, chr from target"
intHuman = pd.read_sql(query1, con=conn)
intHuman['organism'] = "Human"

query2 = "SELECT process as name, count(process) as total from target group by process order by total DESC"
process = pd.read_sql(query2, con=conn)

conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Mouse", unix_socket=unix_socket)

intMouse = pd.read_sql(query1, con=conn)
intMouse['organism'] = "Mouse"

conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Fly", unix_socket=unix_socket)

intFly = pd.read_sql(query1, con=conn)
intFly['organism'] = "Fly"

conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="BuddingYeast", unix_socket=unix_socket)

intBuddingYeast = pd.read_sql(query1, con=conn)
intBuddingYeast['organism'] = "BuddingYeast"

conn.close()

interactomes = intHuman.append([intMouse,intFly,intBuddingYeast])

#Combine together
process = process.to_json(orient='records')
interactomes = interactomes.to_json(orient='records')

json_all = [{'process' : process, 'interactomes':interactomes}]
json_all = json.dumps(json_all)

print ('Content-Type: application/json\n\n')
print (json_all)





