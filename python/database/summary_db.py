#!/usr/bin/env python

import cgi
import cgitb;cgitb.enable()
import json
import pandas as pd
import pymysql
#import simplejson

form = cgi.FieldStorage()
host = form.getvalue('host')
port = form.getvalue('port')
user = form.getvalue('user')
passwd = form.getvalue('passwd')
unix_socket = form.getvalue('unix_socket')

# sessionid = 'test'
# host = "localhost"
# port = 3306
# user = "root"
# passwd = ""
# unix_socket = "/tmp/mysql.sock"

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Human", unix_socket=unix_socket)


query = "select folder as name, count(folder) as total from file_directory group by folder"
dfHuman = pd.read_sql(query, con=conn)
conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Mouse", unix_socket=unix_socket)
dfMouse = pd.read_sql(query, con=conn)
conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="Fly", unix_socket=unix_socket)
dfFly = pd.read_sql(query, con=conn)
conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db="BuddingYeast", unix_socket=unix_socket)
dfBuddingYeast = pd.read_sql(query, con=conn)
conn.close()

dfHuman = dfHuman[dfHuman.name != "User_upload"]
dfMouse = dfMouse[dfMouse.name != "User_upload"]
dfFly = dfFly[dfFly.name != "User_upload"]
dfBuddingYeast = dfBuddingYeast[dfBuddingYeast.name != "User_upload"]

orgs = pd.DataFrame(columns=('name', 'total'))
orgs.loc[0]=['Human',dfHuman.total.sum()]
orgs.loc[1]=['Mouse',dfMouse.total.sum()]
orgs.loc[2]=['Fly',dfFly.total.sum()]
orgs.loc[3]=['BuddingYeast',dfBuddingYeast.total.sum()]


proj = dfHuman.append([dfMouse,dfFly,dfBuddingYeast])

public_proj = proj[proj['name'].str.contains('public', regex=False, case=False, na=False)] 
mito_proj = proj[~proj['name'].str.contains('public', regex=False, case=False, na=False)] 

proj_type = pd.DataFrame(columns=('name', 'total'))
proj_type.loc[0] = ['Analysed data', mito_proj.total.sum()]
proj_type.loc[1] = ['Public data', public_proj.total.sum()]

#Combine together
orgs = orgs.to_json(orient='records')
proj = proj.to_json(orient='records')
proj_type = proj_type.to_json(orient='records')

json_all = [{'orgs' : orgs, 'proj' : proj, 'proj_type' : proj_type}]
json_all = json.dumps(json_all)

print ('Content-Type: application/json\n\n')
print (json_all)





