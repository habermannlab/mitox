#!/usr/bin/env python

import cgi, os, re, sys
import cgitb;cgitb.enable()
import json
import pandas as pd
import pymysql
import numpy as np
import os.path

form = cgi.FieldStorage()
sessionid = form.getvalue('sessionid')
host = form.getvalue('host')
port = form.getvalue('port')
user = form.getvalue('user')
passwd = form.getvalue('passwd')
unix_socket = form.getvalue('unix_socket')

# sessionid = 'BHabermann'
# host = "localhost"
# port = 3306
# user = "root"
# passwd = ""
# unix_socket = "/tmp/mysql.sock"

#fname = '../data/user_uploads/'+sessionid+'/file_directory_'+organism+'.json'
#if os.path.isfile(fname):
#    with open(fname) as json_data:
#        d = json.load(json_data)
#    file_directory = pd.read_json(d,orient='records')
#else:
##connecting to mysql database
conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db='Human', unix_socket=unix_socket)
query = 'SELECT DISTINCT organism, folder FROM file_directory WHERE userID in ("mitox","'+sessionid+'")'
ProjHuman = pd.read_sql(query, con=conn)
conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db='Mouse', unix_socket=unix_socket)
ProjMouse = pd.read_sql(query, con=conn)
conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db='Fly', unix_socket=unix_socket)
ProjFly = pd.read_sql(query, con=conn)
conn.close()

conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db='BuddingYeast', unix_socket=unix_socket)
ProjBuddingYeast = pd.read_sql(query, con=conn)
conn.close()

projects = ProjHuman.append([ProjMouse,ProjFly,ProjBuddingYeast])

projects['proj_type'] = projects['folder'].apply(lambda x: 'Public Data' if 'public' in x else 'Analysed Data')

projects = projects.to_json(orient='records')
    
print ('Content-Type: application/json\n\n')
print (projects)

#file_directory_json = file_directory.to_json(orient='records')
#
#with open(fname, 'w') as fp:
#    json.dump(file_directory_json,fp)


# print ("Content-type: text/html\n")
# print ("<html>")
# print (file_directory)
# print ("</html>")
