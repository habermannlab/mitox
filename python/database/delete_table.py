#!/usr/bin/env python

import cgi, os, re, sys
import cgitb;cgitb.enable()
import json
import pandas as pd
import pymysql

form = cgi.FieldStorage()
project = form.getvalue('project')
jsons = form.getvalue('sampleID')
sampleID = json.loads(jsons)
sessionid = form.getvalue('sessionid')
organism = form.getvalue('organism')
host = form.getvalue('host')
port = form.getvalue('port')
user = form.getvalue('user')
passwd = form.getvalue('passwd')
unix_socket = form.getvalue('unix_socket')

result = "Fail"

try:
    if (project == "User_upload"):
        conn = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=organism, unix_socket=unix_socket)
        cur = conn.cursor()
        query = 'DELETE from file_directory WHERE sampleID in ('+','.join(map("'{0}'".format, sampleID))+') AND userID ="'+sessionid+'"'
        cur.execute(query)
        conn.commit()
        conn.close
        result = json.dumps(["Success"])
    else:
        result = json.dumps(["Fail"])
except:
    result = json.dumps(["Fail"])
  
print ('Content-Type: application/json\n\n')
print (result)
        