<?php 
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}
if ($_GET['sessionid']) { 
        session_id($_GET['sessionid']);
}
session_start();
$id = session_id();

setcookie('mitox_session_id',$id,time() + (86400 * 7));


//Get info for mysql server
$str = file_get_contents('mysql/mysql_info.json');
$json = json_decode($str, true);
$host = $json['host'];
$port = $json['port'];
$user = $json['user'];
$passwd = $json['passwd'];
$unix_socket = $json['unix_socket'];


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>mitoXplorer - Interactome</title>
        <meta charset=utf-8>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script src="./js/jquery.min.js"></script>
        <script src="./js/d3.v3.min.js"></script>
        
        <!-- CSS for interactome page -->
        <link href="./css/main.css" rel="stylesheet" >
        <link href="./css/interactome/interactome.css" rel="stylesheet" >
        
        <link rel="icon" type="image/png" href="img/logos/favicon.png">
    </head>
    
    <body>

<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-header page-scroll">
               <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".sidebar-nav">
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
               </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">mitoXplorer 1.0</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="index.php"></a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="#" style="background-color: #12e2c0;border-radius: 3px;color:white">Interactome</a>
                    </li>
                    <li>
                        <a href="database.php">Database</a>
                    </li>
                    <li>
                        <a href="compare.php">Analysis</a>
                    </li>
                    <li>
                        <a href="upload.php">Upload</a>
                    </li>
                    <li>
                    	<a href="funding.html">Funding</a>
                    </li>
                     <li>
                        <a href="feedback.html">Feedback</a>
                    </li>                   
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                </ul>
            </div>

        </div>

    </nav>
<div id="content" class="container-fluid">
    
<div class="row row-offcanvas row-offcanvas-left">
    
    <div id="sidebar" class="sidenav col-xs-6 col-sm-6 col-md-2 col-lg-2 sidebar-offcanvas" style="padding:20px 10px 0px 10px; text-align: left;">
        
        <div style="margin:10px 10px"><a href="#" onclick="return overview()" style="font-size:22px">Interactomes</a></div>
            
        <div style="margin:0px 10px; padding: 0px 0px;">

            <a href="#" class="interactomeorgs" onclick="return interactome('Human')"><img src="img/interactome/human.png" height="42" width="42">Human</a>
            
            <a href="#" class="interactomeorgs" onclick="return interactome('Mouse')"><img src="img/interactome/mouse.png" height="42" width="42">Mouse</a>
            
            <a href="#" class="interactomeorgs" onclick="return interactome('Fly')"><img src="img/interactome/fly.png" height="42" width="42">Fly</a>
            
            <a href="#" class="interactomeorgs" onclick="return interactome('BuddingYeast')"><img src="img/interactome/yeast.png" height="32" width="32">BuddingYeast</a>
        </div>
        
        <div class="row" id="visside" style="margin:20px 0px;display:none">

            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Search gene by name..." style="font-size:11px">
                <ul class="list-group results"></ul>
            </div>


            <div class="col-md-12 miniTitle" style="margin-top:10px;">
                Legend
            </div>

            <div class="col-md-12 legendText">
                <strong>Color</strong> shows gene regulation
            </div>

            <div class="col-md-12" style="text-align: center">
                <svg width="90" height="30">
                    <rect x="0" y="10" width="28" height="7" fill="#2171b5"></rect>
                    <rect x="28" y="10" width="28" height="7" fill="#BECCAE"></rect>
                    <rect x="56" y="10" width="28" height="7" fill="#C72D0A"></rect>

                    <text x="5" y="26" class="legendText">&gt;1.5</text>
                    <text x="30" y="26" class="legendText"></text>
                    <text x="55" y="26" class="legendText">&lt;-1.5 </text>
                </svg>
            </div>

            <div class="col-md-12 legendText" style="padding-right:0px">
                <strong>Dark Borders</strong> show mutations<br> <strong>Size</strong> shows Log2 fold change
            </div>

            <div class="col-md-12">
                <hr>
            </div>
            <div class="row tip" style="margin-top:20px;font-size:11px">
            </div>

        </div>
    </div>
    
     <div id="main" class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
        <div id="beforedb">
            <h1>Interactome Overview</h1>
            <div style="padding:0px 0px 10px 0px"><span style="font-size:16px">Below is a summary of our interactomes we hosted on mitoXplorer.<br>Click on the graph to download the annotated interactome of mitochondrial genes of different species.<br>Or browse the interactomes with our interactive visualisationby clicking on the side menu.</span></div>
            <div class="col-md-12" id="loadingdb" style="text-align:center"><img id="loading" src="./img/loading.gif"></div>
            <div class="col-md-6" id="histogram" style="display:none">
                <h4>Available Interactomes</h4>
                <div class="col-md-12" id="hist" style="padding-left: 0px; padding-right: 0px;"></div>
            </div>
            <div class="col-md-6" id="barchart" style="display:none">
                <h4>Functional Annotation (Human)</h4>
                <div class="col-md-12" id="chart" style="padding-left: 0px; padding-right: 0px;"></div>
            </div>
        </div>

        <div id="interactome" style="display:none" class="col-lg-12">
            <div id="loadingvis" style="text-align:center;display:none">
            <img src="./img/loading.gif">
            </div>
            <div id="vis" class="vis"></div>
        </div>
    </div>

</div>
</div>
     <script host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/interactome/summary.js"></script>
    <script host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/interactome/AppInteractome.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    
        
</body>
</html>
