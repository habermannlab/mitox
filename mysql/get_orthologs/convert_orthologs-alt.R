# 20180809-2
# Finding orthologs of Celegans
# use this when the other one doesn't work

# Set working directory
setwd("")

# Libraries
require("biomaRt")

# Get the dataset you need
# e.g. celegans_gene_ensemblfor C elegans, mmusculus_gene_ensembl for mouse
# check https://www.bioconductor.org/packages//2.7/bioc/vignettes/biomaRt/inst/doc/biomaRt.pdf
human <- useMart(biomart = "ensembl", dataset = "hsapiens_gene_ensembl")
celegans <- useMart("ensembl", dataset = "celegans_gene_ensembl")

# Read human gene list (in gene symbol)
human_list <- read.table("human_gene_symbol.txt",header=FALSE, sep="\t")

# Get attributes that you need
# !!! The names of attributes change from time to time, make sure you replace the correct one!
# Check the attributes names and description by:
# attributes <- listAttributes(mart=human)

# get list of all elegans genes (since filtering does not work for celegans database)
elegans_list <- getBM(attributes=c("ensembl_gene_id","hsapiens_homolog_associated_gene_name"), mart=celegans)

# Filter and retain only those genes that have a human homologs
elegans_list <- elegans_list[elegans_list[,"hsapiens_homolog_associated_gene_name"] %in% human_list[,1],]

# get attributes for those genes
c_elegans_homologs <- getBM(attributes=c("ensembl_gene_id","wormpep_id", "wormbase_gene","external_gene_name","wikigene_description"), filters="ensembl_gene_id", values = elegans_list[,1], mart=celegans)

# output results
write.table(c_elegans_homologs, file="c_elegans_homologs.txt", quote=FALSE, row.names = FALSE, sep="\t")
