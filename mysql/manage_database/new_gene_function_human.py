# Update gene function table in MySQL (target)
# 1. Update the gene_function file in the folder main_files
# 2. Update the var below (organism, target_path of gene_function file)
# 3. Run the python script

import pandas as pd
import pymysql

organism = "Human"
target_path = "../../main_files/human/human_gene_function.txt"
link_path = "../../main_files/human/human_links.txt"
tmp_path = "../../main_files/"

mysql_f = open("mysql.config","r")
mysql_l = mysql_f.readlines()
username = mysql_l[1].rstrip("\n")
passwd = mysql_l[2].rstrip("\n")
mysql_f.close()

conn = pymysql.connect(host='localhost', port=3306, user=username, passwd=passwd, db=organism, local_infile=1)
cur = conn.cursor()

# First disable foreign key so it doesnt affect the entries in other tables that rely on target table
cur.execute("SET FOREIGN_KEY_CHECKS=0")

# Drop all rows from the target table and import new gene list
cur.execute("TRUNCATE TABLE target")
query = "LOAD DATA LOCAL INFILE '"+target_path+"' INTO TABLE target FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
cur.execute(query)
conn.commit()

# Drop all rows from the links table and import new link list
cur.execute("TRUNCATE TABLE links")
query = "LOAD DATA LOCAL INFILE '"+link_path+"' INTO TABLE links FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
cur.execute(query)
conn.commit()

# Select all genes from the new target table
query = 'SELECT geneID from target'
genes = pd.read_sql(query,con=conn)
genes = genes['geneID'].unique()

# Drop all rows from the target expression and mutation table    
cur.execute("TRUNCATE TABLE target_exp")
cur.execute("TRUNCATE TABLE target_mut")

# Insert rows from the expression table into the target exp table, for those genes that exist in the new target gene list
query = 'INSERT INTO target_exp SELECT userID, sampleID, geneID, normal, abnormal, log2, pvalue FROM expression WHERE geneID in ('+','.join(map("'{0}'".format, genes))+')'
cur.execute(query)
conn.commit()

# Extract info from the mutation table
query = 'SELECT * FROM mutation WHERE geneID in ('+','.join(map("'{0}'".format, genes))+')'
target_mut = pd.read_sql(query,con=conn)

if len(target_mut) > 0:
	# Convert the info into the format that fits the target mutation table
	target_mut['mutation'] = target_mut['chrom'].astype(str)+ ' ' + 	target_mut['pos'].astype(str) + ' ' + target_mut['ref'] + '|' + target_mut['alt'] + ': ' + target_mut['eff'] + ': ' + target_mut['consq']
	target_mut = target_mut[['userID','sampleID','geneID','mutation']]
	target_mut = target_mut.groupby(['userID','sampleID','geneID']).apply(lambda x: '; '.join(x.mutation)).reset_index()
	target_mut.columns = ['userID','sampleID','geneID','mutation']
	target_mut.dropna(subset=['sampleID','geneID'],inplace=True)

	# Push the modified data to MySQL table
	target_mut.to_csv(tmp_path+"target_mut.csv",index=False)
	query = "LOAD DATA LOCAL INFILE '"+tmp_path+"target_mut.csv"+"' IGNORE INTO TABLE target_mut FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;"
	cur.execute(query)
	conn.commit()

# Resume foreign key checks
cur.execute("SET FOREIGN_KEY_CHECKS=1")
conn.commit()

conn.close()


