import requests
import xmltodict
import json
import pandas as pd
import numpy as np
import math

# 20180809
# Getting entrez summary for annotation
# Run R script to get the entrez ID and other gene info

# list of entrez id here
# fname = "xxx.csv"
# with open(fname) as f:
#     content = f.readlines()
# content = [int(x.strip()) for x in content if x != 'NA']

# Or, read the txt file produced from the R scripts
dfin = pd.read_csv("human_ann.txt", sep="\t")

# convert to str as pandas read as int
dfin['entrezgene'] = dfin['entrezgene'].apply(lambda x: str(int(x)) if math.isnan(x) == False else np.nan)
content = dfin['entrezgene'].tolist()

# read info from ncbi (in xml) and convert to dict from xml
r = requests.post("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?", data={'retmode':'xml', 'db':'gene', 'id': content})
o = xmltodict.parse(r.text)

# retrieve info (summary) from the dic
genes = o['Entrezgene-Set']['Entrezgene']
genedict = {}
for gene in genes:
	geneid = gene['Entrezgene_track-info']['Gene-track']['Gene-track_geneid']
	if ('Entrezgene_summary' in gene):
		genedict[geneid] = gene['Entrezgene_summary']
	else:
		genedict[geneid] = np.nan

df = pd.DataFrame.from_dict(genedict, orient="index")
df.reset_index(inplace=True)
df.columns = ['entrezgene','summary']
df['short_summary'] = df['summary'].apply(lambda x: x.split(".")[0])

# merge with info and save output
result = pd.merge(dfin, df, how="left", on='entrezgene')
result.write_csv("huamn_ann_entrez.txt", sep="\t")