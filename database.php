<?php 
if (isset($_COOKIE['mitox_session_id'])) { 
        session_id($_COOKIE['mitox_session_id']);
}
if ($_GET['sessionid']) { 
        session_id($_GET['sessionid']);
}
session_start();
$id = session_id();

setcookie('mitox_session_id',$id,time() + (86400 * 7));


//Get info for mysql server
$str = file_get_contents('mysql/mysql_info.json');
$json = json_decode($str, true);
$host = $json['host'];
$port = $json['port'];
$user = $json['user'];
$passwd = $json['passwd'];
$unix_socket = $json['unix_socket'];


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>mitoXplorer - Database</title>
        <meta charset=utf-8>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script src="./js/jquery.min.js"></script>
        <script src="./js/d3.v3.min.js"></script>
        
        <!-- CSS for database -->
        <link href="./css/main.css" rel="stylesheet" >
        <link href="./css/database/database.css" rel="stylesheet" >
        <link href="./css/database/bootstrap-table.min.css" rel="stylesheet" >
        
        <link rel="icon" type="image/png" href="img/logos/favicon.png">
    </head>
    
    <body>

<nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-header page-scroll">
               <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".sidebar-nav">
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
               </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php">mitoXplorer 1.0</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="index.php"></a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="interactome.php">Interactome</a>
                    </li>
                    <li>
                        <a href="#" style="background-color: #12e2c0;border-radius: 3px;color:white">Database</a>
                    </li>
                    <li>
                        <a href="compare.php">Analysis</a>
                    </li>
                    <li>
                        <a href="upload.php">Upload</a>
                    </li>
                    <li>
                    	<a href="funding.html">Funding</a>
                    </li>
                    <li>
                        <a href="feedback.html">Feedback</a>
                    </li>                    
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                </ul>
            </div>

        </div>

    </nav>
<div id="content" class="container-fluid">
    
<div class="row row-offcanvas row-offcanvas-left">
    
    <div id="sidebar" class="sidenav col-xs-6 col-sm-6 col-md-2 col-lg-2 sidebar-offcanvas" style="padding:20px 0px 0px 0px; text-align: left;">
        
        <!--<div style="margin:10px 10px"><a href="#" onclick="return overview()" style="font-size:22px">Interactomes</a></div>-->

        <div class="panel-group">
        <div style="margin:15px 0px 0px 10px; padding: 0px 0px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse_organism">Organism</a>
                  </h4>
                </div>
                <div id="collapse_organism" class="panel-collapse collapse in">
                  <ul class="list-group">
                    <li class="list-group-item" style="display:none"></li>
                    <li class="list-group-item"><a href="#" onclick="return updateProjtype('Human')">Human</a></li>
                    <li class="list-group-item"><a href="#" onclick="return updateProjtype('Mouse')">Mouse</a></li>
                    <li class="list-group-item"><a href="#" onclick="return updateProjtype('Fly')">Fly</a></li>
                    <li class="list-group-item"><a href="#" onclick="return updateProjtype('BuddingYeast')">BuddingYeast</a></li>
                  </ul>
                </div>
            </div>
        </div>

        <div style="margin:10px 0px 0px 10px; padding: 0px 0px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse_projtype">Type</a>
                  </h4>
                </div>
                <div id="collapse_projtype" class="panel-collapse collapse">
                  <ul class="list-group" id="list_projtype">
                    <li class="list-group-item" style="display:none"></li>
                  </ul>
                </div>
            </div>
        </div>

        <div style="margin:10px 0px 0px 10px; padding: 0px 0px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse_proj">Project</a>
                  </h4>
                </div>
                <div id="collapse_proj" class="panel-collapse collapse">
                  <ul class="list-group" id="list_proj">
                    <li class="list-group-item" style="display:none"></li>
                  </ul>
                </div>
            </div>
        </div>
        </div>

    </div>
    
      <div id="main" class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
        <div id="beforedb">
            <h1>Database Overview</h1>
            <div style="padding:0px 0px 10px 0px"><span style="font-size:16px">Below is a summary of the public data we hosted on mitoXplorer.<br>Explore the data by clicking on the side menu.</span></div>
            
            <div class="col-md-12" id="loadingdb" style="text-align:center"><img id="loading" src="./img/loading.gif"></div>

            <div class="col-md-12" id="pies" style="display:none">
                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                    <h4>By Organisms</h4>
                    <div class="col-md-12" id="pie1" style="padding-left: 0px; padding-right: 0px; padding-top:20px"></div>
                </div>
                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                    <h4>By Project Types</h4>
                    <div class="col-md-12" id="pie2" style="padding-left: 0px; padding-right: 0px; padding-top:20px"></div>       
                </div>
                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                    <h4>By Projects</h4>
                    <div class="col-md-12" id="pie3" style="padding-left: 0px; padding-right: 0px; padding-top:20px"></div>       
                </div>
                <div class="col-md-12" id="pcinfo"></div>
            </div>
        </div>
        <div id="db" style="display:none" class="col-lg-12">
            <h2 id="database-head">s</h2>
            <div id="database-ref" class = "col-md-12" style="padding-left: 0px; padding-bottom: 10px; display:none"></div>
            <div class="col-md-12" style='font-size:14px'>
            <b>Quick Browse</b> <span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="font-size: 1.2em"></span><br>Check out the expression and mutation profile of individual datasets in a new window<br><br>
            <b>Comparative Analysis</b> <span class="glyphicon glyphicon-ok" aria-hidden="true" style="font-size: 1.2em"></span><br>Select up to 6 datasets from the table and click compare to start the Data Analysis Portal<br><br>
            <div id="buttons"><button id="readygo" class="btn btn-success" style="margin-right:10px"> Compare </button></div>
            <div style = "display:none;margin-top:10px" id = "warning"></div>
            </div>
            <div id="loading" style="text-align:center">
            <img src="./img/loading.gif">
            </div>
            <div class="row body" id="userfiles">
            </div><br><br>
        </div>
    </div> 

</div>
</div>
     <script session-id="<?php echo $id; ?>" host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/database/userFiles.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/database/bootstrap-table.min.js"></script>
    
    <script host="<?php echo $host; ?>" port="<?php echo $port; ?>" user="<?php echo $user; ?>" passwd="<?php echo $passwd; ?>" unix_socket="<?php echo $unix_socket; ?>" src="js/database/summary_db.js"></script>
        
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
            </div>

            <div class="modal-body">
                <p>You are about to delete one track, this procedure is irreversible.</p>
                <p>Do you want to proceed?</p>
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger btn-ok" id="deleteSamples"  data-dismiss="modal">Delete</button>
            </div>
        </div>
    </div>
</div> 
        
</body>
</html>
