sampleID	Gene-KO	Data-type	Dataset	Experiment
Control_12w	WT	Transcriptome	Control_12w	Timecourse
Control_14w_p	WT	Proteome	Control_14w_p	Timecourse
Control_16w	WT	Transcriptome	Control_16w	Timecourse
Control_18w_p	WT	Proteome	Control_18w_p	Timecourse
Control_19w_p	WT	Proteome	Control_19w_p	Timecourse
Control_20w	WT	Transcriptome	Control_20w	Timecourse
Control_23w_p	WT	Proteome	Control_23w_p	Timecourse
Control_4w_p	WT	Proteome	Control_4w_p	Timecourse
Control_8w_p	WT	Proteome	Control_8w_p	Timecourse
Control_9w	WT	Transcriptome	Control_9w	Timecourse
Control_9w_p	WT	Proteome	Control_9w_p	Timecourse
Lrpprc_KO	Lrpprc	Transcriptome	Lrpprc_KO	Gene-KO
Lrpprc_KO_10w_p	Lrpprc	Proteome	Lrpprc_KO_10w_p	Timecourse
Lrpprc_KO_2w_p	Lrpprc	Proteome	Lrpprc_KO_2w_p	Timecourse
Lrpprc_KO_3_4w_p	Lrpprc	Proteome	Lrpprc_KO_3_4w_p	Timecourse
Lrpprc_KO_5w_p	Lrpprc	Proteome	Lrpprc_KO_5w_p	Timecourse
Lrpprc_KO_7w_p	Lrpprc	Proteome	Lrpprc_KO_7w_p	Timecourse
Lrpprc_KO_p	Lrpprc	Proteome	Lrpprc_KO_p	Gene-KO
Mterf4_KO	Mterf4	Transcriptome	Mterf4_KO	Gene-KO
Mterf4_KO_p	Mterf4	Proteome	Mterf4_KO_p	Gene-KO
Polrmt_KO	Polrmt	Transcriptome	Polrmt_KO	Gene-KO
Polrmt_KO_p	Polrmt	Proteome	Polrmt_KO_p	Gene-KO
Tfam_KO	Tfam	Transcriptome	Tfam_KO	Gene-KO
Tfam_KO_p	Tfam	Proteome	Tfam_KO_p	Gene-KO
Twnk_KO	Twnk	Transcriptome	Twnk_KO	Gene-KO
Twnk_KO_p	Twnk	Proteome	Twnk_KO_p	Gene-KO



























